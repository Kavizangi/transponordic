<div class="master-wrapper">
    <!-- .master-slider start -->
    <div id="masterslider" class="master-slider ms-skin-default mb-0">
        <!-- slide start -->
        <div class="ms-slide" data-delay="5">
            <img src="<?php echo BASE_URL?>/assets/frontend/masterslider/blank.gif" data-src="<?php echo BASE_URL?>/assets/frontend/masterslider/slider3.jpg" alt=""/>
        </div><!-- slide end -->

        <!-- slide start -->
        <div class="ms-slide" data-delay="5">
            <img src="<?php echo BASE_URL?>/assets/frontend/masterslider/blank.gif" data-src="<?php echo BASE_URL?>/assets/frontend/masterslider/slider4.jpg" alt=""/>
        </div><!-- slide end -->
    </div><!-- .master-slider end -->
</div>