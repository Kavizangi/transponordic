
<div id="footer-wrapper" class="footer-dark">
    <footer id="footer">
        <div class="container">
            <div class="row">
                <ul class="col-md-3 col-sm-6 footer-widget-container clearfix">
                    <!-- .widget.widget_text -->
                    <li class="widget widget_newsletterwidget">
                        <div class="title">
                            <h3>newsletter subscribe</h3>
                        </div>

                        <p>
                            Subscribe to our newsletter and we will
                            inform you about newest projects and promotions.
                        </p>

                        <br />

                        <form class="newsletter">
                            <input class="email" type="email" placeholder="Your email...">
                            <input type="submit" class="submit" value="">
                        </form>
                    </li><!-- .widget.widget_newsletterwidget end -->
                </ul><!-- .col-md-3.footer-widget-container end -->

                <ul class="col-md-3 col-sm-6 footer-widget-container">
                    <!-- .widget-pages start -->
                    <li class="widget widget_pages">
                        <div class="title">
                            <h3>quick links</h3>
                        </div>

                        <ul>
                            <li><a href="<?php echo BASE_URL?>/index.php">Home</a></li>
                            <li><a href="<?php echo BASE_URL?>/track-order.php">Track your Order</a></li>
                            <li><a href="<?php echo BASE_URL?>/about.php">About</a></li>
                            <li><a href="<?php echo BASE_URL?>/services.php">Services</a></li>
                            <li><a href="<?php echo BASE_URL?>/contact.php">Contact Us</a></li>
                            <li><a href="<?php echo BASE_URL?>/sign-up.php">Register</a></li>
                            <li><a href="<?php echo BASE_URL?>/login.php">Login</a></li>
                        </ul>
                    </li><!-- .widget-pages end -->
                </ul><!-- .col-md-3.footer-widget-container end -->

                <ul class="col-md-3 col-sm-6 footer-widget-container">
                    <!-- .widget-pages start -->
                    <li class="widget widget_pages">
                        <div class="title">
                            <h3>Our services</h3>
                        </div>

                        <ul>
                            <li><a href="<?php echo BASE_URL?>/services.php?t=forwarding">Forwarding and Consolidation Service</a></li>
                            <li><a href="<?php echo BASE_URL?>/services.php?t=sole">Full Container Load - Sole Use Service</a></li>
                            <li><a href="<?php echo BASE_URL?>/services.php?t=shared">Less Than Container Load - Shared Space Service</a></li>
                        </ul>
                    </li><!-- .widget-pages end -->
                </ul><!-- .col-md-3.footer-widget-container end -->

                <ul class="col-md-3 col-sm-6 footer-widget-container">
                    <li class="widget widget-text">
                        <div class="title">
                            <h3>Contact us</h3>
                        </div>

                        <address>
                            <?php echo $core->c_address;?>, <br />
                            <?php echo $core->c_city;?>, <?php echo $core->c_country;?>
                        </address>

                        <span class="text-big">
                           <?php echo $core->cell_phone;?>
                        </span>
                        <br />

                        <a href="mailto:<?php echo $core->site_email;?>"><?php echo $core->site_email;?></a>
                        <br />
                        <ul class="footer-social-icons">
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-google-plus"></a></li>
                        </ul><!-- .footer-social-icons end -->
                    </li><!-- .widget.widget-text end -->
                </ul><!-- .col-md-3.footer-widget-container end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </footer><!-- #footer end -->

    <div class="copyright-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p><?php echo strtoupper($core->site_name);?> <?php echo date('Y');?>. All RIGHTS RESERVED.</p>
                </div><!-- .col-md-6 end -->

            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .copyright-container end -->

    <a href="#" class="scroll-up">Scroll</a>
</div><!-- #footer-wrapper end -->

<script src="<?php echo BASE_URL?>/assets/frontend/js/bootstrap.min.js"></script><!-- .bootstrap script -->
<script src="<?php echo BASE_URL?>/assets/frontend/js/jquery.srcipts.min.js"></script><!-- modernizr, retina, stellar for parallax -->
<script src="<?php echo BASE_URL?>/assets/frontend/owl-carousel/owl.carousel.min.js"></script><!-- Carousels script -->
<script src="<?php echo BASE_URL?>/assets/frontend/masterslider/masterslider.min.js"></script><!-- Master slider main js -->
<script src="<?php echo BASE_URL?>/assets/frontend/js/jquery.dlmenu.min.js"></script><!-- for responsive menu -->
<script src="<?php echo BASE_URL?>/assets/frontend/style-switcher/styleSwitcher.js"></script><!-- styleswitcher script -->
<script src="<?php echo BASE_URL?>/assets/frontend/js/include.js"></script><!-- custom js functions -->

</body>
</html>
