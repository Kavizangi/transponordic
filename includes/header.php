<?php
    define("_VALID_PHP", false);

    require_once("init.php");
    define("BASE_URL", SITEURL);
    define("RECAPTCHA_KEY", "");
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $core->site_name;?> - <?php echo isset($page_title) ? $page_title : 'Home';?></title>
    <meta name="description" content="Trucking is transportation and Logistics company based in Denmark">
    <meta name="author" content="http://phppot.co.ke">
    <meta name="keywords" content="transponordic,shipment,cargo">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/png" sizes="56x56" href="<?php echo BASE_URL?>/uploads/<?php echo $core->favicon ? $core->favicon : 'favicon.png'?>">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/css/bootstrap.css"/><!-- bootstrap grid -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/masterslider/style/masterslider.css" /><!-- Master slider css -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/masterslider/skins/default/style.css" /><!-- Master slider default skin -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/css/animate.css"/><!-- animations -->
    <link rel='stylesheet' href='<?php echo BASE_URL?>/assets/frontend/owl-carousel/owl.carousel.css'/><!-- Client carousel -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/css/style.css"/><!-- template styles -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/css/color-default.css"/><!-- template main color -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/css/retina.css"/><!-- retina ready styles -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/css/responsive.css"/><!-- responsive styles -->

    <!-- Google Web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,800,700,600' rel='stylesheet' type='text/css'>

    <!-- Font icons -->
    <link rel="stylesheet" href="<?php echo BASE_URL?>/assets/frontend/icon-fonts/font-awesome-4.3.0/css/font-awesome.min.css"/>
    <script src="<?php echo BASE_URL?>/assets/frontend/js/jquery-2.1.4.min.js"></script><!-- jQuery library -->
    <style>
        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .alert-success {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }
        .alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }
    </style>
</head>

<body>
<div class="header-wrapper">

    <header id="header" class="header-style02">
        <div class="header-inner">
            <!-- .container start -->
            <div class="container">
                <!-- .main-nav start -->
                <div class="main-nav">
                    <!-- .row start -->
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-default nav-left" role="navigation">

                                <!-- .navbar-header start -->
                                <div class="navbar-header">
                                    <div class="logo">
                                        <a href="<?php echo BASE_URL?>">
                                            <img src="<?php echo BASE_URL?>/uploads/<?php echo $core->logo ? $core->logo : 'logo.png'?>" style="height: 80px;" alt="Transponordic"/>
                                        </a>
                                    </div><!-- .logo end -->
                                </div><!-- .navbar-header start -->

                                <!-- MAIN NAVIGATION -->
                                <div class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav">
                                        <li><a href="<?php echo BASE_URL?>/index.php">Home</a></li>
                                        <li><a href="<?php echo BASE_URL?>/about.php">About Us</a></li>

                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Services</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo BASE_URL?>/services.php?t=forwarding">Forwarding and Consolidation Service</a></li>
                                                <li><a href="<?php echo BASE_URL?>/services.php?t=sole">Full Container Load - Sole Use Service</a></li>
                                                <li><a href="<?php echo BASE_URL?>/services.php?t=shared">Less Than Container Load - Shared Space Service</a></li>
                                            </ul><!-- .dropdown-menu end -->
                                        </li><!-- .dropdown end -->
                                        <li><a href="<?php echo BASE_URL?>/contact.php">Contact Us</a></li>
                                        <li><a href="<?php echo BASE_URL?>/track-order.php">Tracking</a></li>
                                        <?php
                                        if (!$user->logged_in){
                                        ?>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Account</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo BASE_URL?>/sign-up.php">Register</a></li>
                                                    <li><a href="<?php echo BASE_URL?>/login.php">Login</a></li>
                                                </ul><!-- .dropdown-menu end -->
                                            </li><!-- .dropdown end -->
                                            <li><a href="<?php echo BASE_URL?>/login.php">Request Now</a></li>
                                        <?php
                                        }else{
                                        ?>
                                            <li><a href="<?php echo BASE_URL.($user->is_Admin() ? '/dashboard/index.php' : '/dashboard/client.php');?>">My Dashboard</a></li>
                                            <li><a href="<?php echo BASE_URL?>/dashboard/client.php">Request Now</a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>

                                    <!-- RESPONSIVE MENU -->
                                    <div id="dl-menu" class="dl-menuwrapper">
                                        <button class="dl-trigger">Open Menu</button>

                                        <ul class="dl-menu">
                                            <ul class="dl-menu">
                                                <li><a href="<?php echo BASE_URL?>/index.php">Home</a></li>
                                                <li><a href="<?php echo BASE_URL?>/track-order.php">Tracking</a></li>
                                                <li><a href="<?php echo BASE_URL?>/about.php">About</a></li>
                                                <li><a href="<?php echo BASE_URL?>/contact.php">Contact Us</a></li>
                                                <li class="dropdown">
                                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Services</a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="<?php echo BASE_URL?>/services.php?t=forwarding">Forwarding and Consolidation Service</a></li>
                                                        <li><a href="<?php echo BASE_URL?>/services.php?t=sole">Full Container Load - Sole Use Service</a></li>
                                                        <li><a href="<?php echo BASE_URL?>/services.php?t=shared">Less Than Container Load - Shared Space Service</a></li>
                                                    </ul><!-- .dropdown-menu end -->
                                                </li><!-- .dropdown end -->
                                                <?php
                                                if (!$user->logged_in){
                                                ?>
                                                    <li class="dropdown">
                                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Account</a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?php echo BASE_URL?>/sign-up.php">Register</a></li>
                                                            <li><a href="<?php echo BASE_URL?>/login.php">Login</a></li>
                                                        </ul><!-- .dropdown-menu end -->
                                                    </li><!-- .dropdown end -->
                                                    <li><a href="<?php echo BASE_URL?>/login.php">Request Now</a></li>
                                                <?php
                                                }else{
                                                ?>
                                                    <li><a href="<?php echo BASE_URL.($user->is_Admin() ? '/dashboard/index.php' : '/dashboard/client.php');?>">My Dashboard</a></li>
                                                    <li><a href="<?php echo BASE_URL?>/dashboard/client.php">Request Now</a></li>
                                                <?php
                                                }
                                                ?>
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
