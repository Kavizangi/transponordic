<?php
    include "includes/header.php";
    include "includes/home-slider.php";
?>
<div class="page-content parallax parallax01 dark mb-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action clearfix">
                    <div class="text">
                        <h2>TranspoNordic Logistics (TNL) | Service to Humanity</h2>
                        <p>
                            TranspoNordic Logistics (TNL) is an Africa focused courier company that assists private individuals and transport companies  in the movement of their goods to and from Africa.
                            We have specialized services in car and container shipments, forwarding of goods purchased online to their respective buyers, and household and personal effects goods to loved ones in Africa. With our partners across Africa and Europe,
                            we have unparallel delivery service to serve Africa.
                        </p>
                    </div><!-- .text end -->

                    <a href="<?php echo BASE_URL?>/login.php" class="btn btn-big">
                        <span>Request Now</span>
                    </a>
                </div><!-- .call-to-action end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content.parallax end -->

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="custom-heading">
                    <h2>Our services</h2>
                </div><!-- .custom-heading end -->

                <img src="<?php echo BASE_URL?>/assets/frontend/img/pics/illustration02.png" class="float-right" alt=""/>

                <ul class="fa-ul">
                    <li>
                        <i class="fa fa-li fa-long-arrow-right"></i>
                        Full Container Load - Sole Use Service
                    </li>

                    <li>
                        <i class="fa fa-li fa-long-arrow-right"></i>
                        Less Than Container Load - Shared Space Service
                    </li>

                    <li>
                        <i class="fa fa-li fa-long-arrow-right"></i>
                        Forwarding and Consolidation Service
                    </li>
                </ul><!-- .fa-ul end -->
            </div><!-- .col-md-4 end -->

            <div class="col-md-6 col-sm-6 clearfix">
                <div class="custom-heading">
                    <h2>Full Container Load - Sole Use Service</h2>
                </div><!-- .custom-heading end -->

                <p>
                    Do you have enough goods to fill 20 feet or 40 feet container?
                    Goods that will usually required a whole container called a ‘full container load’ (FCL) or sole-use shipment.
                    Our full container load service is designed for customers with goods to occupy 20 or 40 feet container.
                    We have extensive experience in commercial invoicing and packing list issuance to individual shippers.
                    We also assist shippers to obtain inspection clearance documents where they are required.
                </p>

                <a href="services.php" class="read-more">
                    <span>
                        View all services
                        <i class="fa fa-chevron-right"></i>
                    </span>
                </a>
            </div><!-- .col-md-4 end -->

        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content end -->

<div class="page-content custom-bkg bkg-light-blue mb-70">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <img class="img-fixed-bottom" src="<?php echo BASE_URL?>/assets/frontend/img/pics/illustration03.png" alt="slider"/>
            </div><!-- .col-md-8 end -->

            <div class="col-md-5">
                <div class="custom-heading">
                    <h2>Why Choose TranspoNordic Logistics (TNL)</h2>
                </div><!-- .custom-heading end -->

                <ul class="fa-ul large-icons">
                    <li>
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <i class="fa fa-check-circle"></i>
                        </div>

                        <div class="li-content">
                            <h3>Fast Worldwide delivery</h3>

                            <p>
                                From Europe to Africa?
                                We offer fast, reliable and accurate
                                worldwide delivery directly to your
                                doors, factory and warehouses.
                            </p>
                        </div><!-- .li-content end -->
                    </li>

                    <li>
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <i class="fa fa-check-circle"></i>
                        </div>

                        <div class="li-content">
                            <h3>Safety & Compliance</h3>

                            <p>
                                Safety of your cargo is one of our top
                                priorities. Every package is handled
                                with most care by our trained and high
                                skilled personnel. You can be sure that
                                your cargo will travel and arrive safely.
                            </p>
                        </div><!-- .li-content end -->
                    </li>
                </ul><!-- .fa-ul .fa-ul-large end -->
            </div><!-- .col-md-4 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content end -->

<?php
include "includes/footer.php";
?>
<script>
    /* <![CDATA[ */
    jQuery(document).ready(function ($) {
        'use strict';

        // MASTER SLIDER START
        var slider = new MasterSlider();
        slider.setup('masterslider', {
            width: 1140, // slider standard width
            height: 530, // slider standard height
            layout: "fullwidth",
            centerControls: false,
            loop: true,
            speed: 40,
            autoplay: true
            // more slider options goes here...
            // check slider options section in documentation for more options.
        });
        // adds Arrows navigation control to the slider.
        slider.control('arrows');


        $('#client-carousel').owlCarousel({
            items: 6,
            loop: true,
            margin: 30,
            responsiveClass: true,
            mouseDrag: true,
            dots: false,
            responsive: {
                0: {
                    items: 2,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    responsiveClass: true
                },
                600: {
                    items: 3,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    responsiveClass: true
                },
                1000: {
                    items: 6,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    responsiveClass: true,
                    mouseDrag: true
                }
            }
        });

    });
    /* ]]> */
</script>
