<?php
include "includes/header.php";

if ($user->logged_in){
    if ($user->is_Admin()){
        redirect_to("dashboard/index.php");
    }else{
        redirect_to("dashboard/client.php");
    }
}

if (isset($_POST['doLogin']))
    : $result = $user->login($_POST['username'], $_POST['password']);

    /* Login Successful */
    if ($result)
        if($_SESSION['userlevel'] == 9)
            :	redirect_to("dashboard/index.php");
        elseif($_SESSION['userlevel'] == 1)
            :	redirect_to("dashboard/client.php");

        elseif($_SESSION['userlevel'] == 2)
            :	redirect_to("dashboard/index.php");

        else:
            if($_SESSION['userlevel'] == 3)
                :	redirect_to("dashboard/dash_driver.php");
            endif;
        endif;
endif;


?>
<!-- .page-title start -->
<div class="page-title-style01 page-title-negative-top pt-bkg08" style="padding-top: 255px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Sign In</h1>

                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>You are here:</li>
                        <li>
                            <a href="<?php echo BASE_URL;?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL;?>/login.php">Sign In</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-title-style01.page-title-negative-top end -->

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-6">
            <div id="msgholder2" style="color: red">
                <?php print Filter::$showMsg;?>
            </div>
                <form class="login-form"  method="post" name="login_form" id="login-form">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group position-relative">
                                <label><?php echo $lang['left115'] ?> <span class="text-danger">*</span></label>
                                <i class="mdi mdi-account ml-3 icons"></i>
                                <input type="text" class="form-control pl-5" placeholder="<?php echo $lang['left116'] ?>" name="username" id="username" required="">
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group position-relative">
                                <label><?php echo $lang['left117'] ?> <span class="text-danger">*</span></label>
                                <i class="mdi mdi-key ml-3 icons"></i>
                                <input type="password" class="form-control pl-5" placeholder="<?php echo $lang['left118'] ?>" name="password" id="password" required="">
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <p class="float-right forgot-pass"><a href="forgot-password.php" class="text-dark font-weight-bold"><?php echo $lang['left119'] ?></a></p>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1"><?php echo $lang['left120'] ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 mb-0">
                            <button class="btn btn-primary" style="color: white;padding: 2%;">Login</button>
                            <input name="doLogin" type="hidden" value="1" />
                        </div>
                        <br><br>
                        <div class="col-12 text-center">
                            <p class="mb-0 mt-3"><small class="text-dark mr-2"><?php echo $lang['left122'] ?></small> <a href="sign-up.php" class="text-dark font-weight-bold" style="color: #006db7;"><?php echo $lang['left123'] ?></a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
include "includes/footer.php";
?>
