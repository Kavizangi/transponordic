<?php
include "includes/header.php";

if ($user->logged_in){
    if ($user->is_Admin()){
        redirect_to("dashboard/index.php");
    }else{
        redirect_to("dashboard/client.php");
    }
}

?>
<script src="<?php echo BASE_URL?>/assets/assets/js/global.js"></script>
<!-- .page-title start -->
<div class="page-title-style01 page-title-negative-top pt-bkg08" style="padding-top: 255px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Sign Up</h1>

                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>You are here:</li>
                        <li>
                            <a href="<?php echo BASE_URL;?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL;?>/sign-up.php">Sign Up</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-title-style01.page-title-negative-top end -->

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-6">
                <?php if(!$core->reg_allowed):?>
                    <?php echo Filter::msgAlert("<span>Alert!</span>".$lang['langs_010133']."");?>
                <?php elseif($core->user_limit !=0 and $core->user_limit == $numusers):?>
                    <?php echo Filter::msgAlert("<span>Alert!</span>".$lang['langs_010134']."");?>
                <?php else:?>
                    <div class="login_page bg-white shadow rounded p-4">
                        <div class="text-center">
                            <h4 class="mb-4"><?php echo $lang['left136'] ?></h4>
                            <p><?php echo $lang['left137'] ?></p>
                        </div>
                        <div id="msgholder" style="color: red;"></div>
                        <form class="login-form" id="admin_form" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label><?php echo $lang['left138'] ?> <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-account ml-3 icons"></i>
                                        <input type="text" class="form-control pl-5" placeholder="<?php echo $lang['left139'] ?>" name="fname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label><?php echo $lang['left140'] ?> <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-account ml-3 icons"></i>
                                        <input type="text" class="form-control pl-5" placeholder="<?php echo $lang['left141'] ?>" name="lname">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label><?php echo $lang['left142'] ?> <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-mail-ru ml-3 icons"></i>
                                        <input type="email" class="form-control pl-5" placeholder="<?php echo $lang['left143'] ?>" name="email" >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label><?php echo $lang['left144'] ?> <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-account ml-3 icons"></i>
                                        <input type="text" class="form-control pl-5" placeholder="<?php echo $lang['left145'] ?>" name="username">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label><?php echo $lang['left146'] ?> <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-key ml-3 icons"></i>
                                        <input type="password" class="form-control pl-5" placeholder="<?php echo $lang['left147'] ?>" name="pass">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label><?php echo $lang['left148'] ?> <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-key ml-3 icons"></i>
                                        <input type="password" class="form-control pl-5" name="pass2" placeholder="<?php echo $lang['left149'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><span class="text-danger"></span></label>
                                        <span class="badge-light"><img src="lib/captcha.php" alt="" class="captcha-append" /></span>
                                    </div>
                                </div> <!-- /.col- -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?php echo $lang['left162'] ?> <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="captcha" placeholder="<?php echo $lang['left163'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1" name="terms" value="yes" >
                                            <label class="custom-control-label" for="customCheck1"><?php echo $lang['left164'] ?> <a href="#" class="text-primary"> <?php echo $lang['left165'] ?></a></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary rounded w-100" name="dosubmit" style="color: white;padding: 2%;"><?php echo $lang['left166'] ?></button>
                                    <input name="locker" type="hidden" value="<?php echo generarCodigo(6); ?>" />
                                    <input name="doRegister" type="hidden" value="1" />
                                </div>
                                <div class="col-lg-12 mt-4 text-center"></div>
                                <div class="mx-auto">
                                    <p class="mb-0 mt-3"><small class="text-dark mr-2"><?php echo $lang['left167'] ?> </small> <a href="index.php" class="text-dark font-weight-bold" style="color: #006db7;"><?php echo $lang['left168'] ?></a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <script type="text/javascript">
                        // <![CDATA[
                        function showResponse(msg) {
                            hideLoader();
                            if (msg == 'OK') {
                                result = "<div class=\"alert alert-success\"><p><span class=\"icon-ok-sign\"><\/span><i class=\"close icon-remove-circle\"></i><span>Success!<\/span> You have successfully registered! and an email was sent with your details<\/p><\/div>";
                                $("#fullform").hide();
                            } else {
                                result = msg;
                            }
                            $(this).html(result);
                            $("html, body").animate({
                                scrollTop: 0
                            }, 600);
                        }
                        $(document).ready(function () {
                            let options = {
                                target: "#msgholder",
                                beforeSubmit: showLoader,
                                success: showResponse,
                                url: "ajax/user.php",
                                resetForm: 0,
                                clearForm: 0,
                                data: {
                                    processContact: 1
                                }
                            };
                            $("#admin_form").ajaxForm(options);
                        });
                        // ]]>
                    </script>
                <?php endif;?>
            </div>

        </div>
    </div>
</div>
<?php
include "includes/footer.php";
?>
<script>
    function showLoader() {
        $("#loader").fadeIn(200);
    }
    function hideLoader() {
        $("#loader").fadeOut(200);
    }
</script>
