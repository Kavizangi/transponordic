<?php
include "includes/header.php";
?>
<!-- .page-title start -->
<div class="page-title-style01 page-title-negative-top pt-bkg08" style="padding-top: 255px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Track Order</h1>

                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>You are here:</li>
                        <li>
                            <a href="<?php echo BASE_URL;?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL;?>/track-order.php">Track Order</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-title-style01.page-title-negative-top end -->

<div class="page-content">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="mr-lg-5">
                    <img src="<?php SITEURL ?>assets/theme_deprixa/images/user/track.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="login-page bg-white shadow rounded p-4">
                    <div class="text-center">
                        <br/>
                        <br/>
                        <h4><span class="text-primary"><?php echo $lang['left127'] ?> </span> <br> <?php echo $lang['left128'] ?><span>.</span></h4>
                    </div>
                    <form class="login-form" action="track-order-details.php" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group position-relative">
                                    <label><?php echo $lang['left129'] ?> <?php echo $core->site_name ?><span class="text-primary">.</span></label>
                                    <i class="mdi mdi-cube-send ml-3 icons"></i>
                                    <textarea name="order_inv" placeholder="<?php echo $lang['left130'] ?>"  id="comments" rows="4" class="form-control pl-5" required></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" style="color: white;padding: 2%;" name="submit" class="btn btn-primary"><i class="mdi mdi-cube-outline ml-3 icons"></i> <?php echo $lang['left131'] ?></button>
                                </div><!--end col-->
                            </div><!--end row-->
                        </div>
                    </form>
                </div><!---->
            </div> <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</div>
<?php
include "includes/footer.php";
?>
