<?php
include "includes/header.php";
$type=isset($_GET['t']) ? $_GET['t'] : 'forwarding';
if ( (!in_array($type,['sole','shared','forwarding'])) ){
    $type="sole";
}
?>
<!-- .page-title start -->
<div class="page-title-style01 page-title-negative-top pt-bkg08" style="padding-top: 255px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>TranspoNordic Logistics (TNL) - Our Services</h1>

                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>You are here:</li>

                        <li>
                            <a href="<?php echo BASE_URL;?>">Home</a>
                        </li>

                        <li>
                            <a href="javascript:void();">Our Services</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div>

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="custom-heading">
                    <h2><?php echo $type=="sole" ? "Full Container Load - Sole Use Service" : ($type=="shared" ? "Less Than Container Load - Shared Space Service" : "Forwarding and Consolidation Service");?></h2>
                </div><!-- .custom-heading end -->
                    <?php
                    if ($type == "forwarding"){
                    ?>
                <p>This is one of our innovative services to relieve the pain of African online buyers in the European Union.
                    With this service, you can buy online in any EU country from some selected African countries.
                    You can use our warehouse as your forwarding address.
                    TNL will receive the package on the buyers’ behalf for 30 days without storage fees and ship the package to the buyer later.
                    In case a buyer wants to purchase more products to consolidate the packages, the received packages with more than 30 days in TNL’s custody will attract a small fee.
                    After enough packages have been received from online outlets or other places,  shipment can be initiated by the buyers and we will ship the goods to the buyers at their destination.
                    Note. All purchases should be within the EU.
                    <a href="dashboard/client.php" style="color: #006db7;"> Request your address</a>.
                </p>
                    <?php
                    }else if ($type == "sole"){
                    ?>
                <p> Do you have enough goods to fill 20 feet or 40 feet container?  Goods that will usually required a whole container called a ‘full container load’ (FCL) or sole-use shipment.
                    Our full container load service is designed for customers with goods to occupy 20 or 40 feet container.
                    We have extensive experience in commercial invoicing and packing list issuance to individual shippers.
                    We also assist shippers to obtain inspection clearance documents where they are required.
                    <a href="dashboard/client.php" style="color: #006db7;"> Request a container here</a>.
                </p>
                    <?php
                    } else if ($type == "shared"){
                    ?>
                <p>
                    This service is for people with smaller amounts of cargo and where several customers can share the space of a single container.
                    This is called a ‘less than container load’ (LCL) or a part-load shipment.
                    With this service, we can mobilize goods from several individuals and SMEs and ship the goods to their destinations in Africa.
                    Through our shared space service and with the help from our European courier partners (GLS,  Bring, and DB Schenker) we can assist Africans in European Union (EU) or European Economic Area (EEA) to send goods to their families and love ones in Africa.
                    <a href="dashboard/client.php" style="color: #006db7;"> Order your space here</a>.
                </p>
                    <?php
                    }
                    ?>

            </div><!-- .col-md-8 end -->

        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content end -->

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="custom-heading">
                    <h2>Why Choose TranspoNordic Logistics (TNL)</h2>
                </div><!-- .custom-heading end -->

                <ul class="fa-ul large-icons">
                    <li>
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <i class="fa fa-check-circle"></i>
                        </div>

                        <div class="li-content">
                            <h3>Fast Worldwide delivery</h3>

                            <p>
                                From Europe to Africa?
                                We offer fast, reliable and accurate
                                worldwide delivery directly to your
                                doors, factory and warehouses.
                            </p>
                        </div>
                    </li>

                    <li>
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <i class="fa fa-check-circle"></i>
                        </div>

                        <div class="li-content">
                            <h3>Safety & Compliance</h3>

                            <p>
                                Safety of your cargo is one of our top
                                priorities. Every package is handled
                                with most care by our trained and high
                                skilled personnel. You can be sure that
                                your cargo will travel and arrive safely.
                            </p>
                        </div><!-- .li-content end -->
                    </li>
                </ul><!-- .fa-ul .fa-ul-large end -->
            </div><!-- .col-md-5 end -->

            <div class="col-md-7 triggerAnimation animated" data-animate="fadeInRight">
                <img src="<?php echo BASE_URL?>/assets/frontend/img/pics/img34.jpg" alt="trucking"/>
            </div><!-- .col-md-7 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content end -->

<?php
include "includes/footer.php";
?>
