<?php
include "includes/header.php";

if (isset($_POST['doLogin']))
    : $result = $user->login($_POST['username'], $_POST['password']);

    /* Login Successful */
    if ($result)
        if($_SESSION['userlevel'] == 9)
            :	redirect_to("dashboard/index.php");
        elseif($_SESSION['userlevel'] == 1)
            :	redirect_to("dashboard/client.php");

        elseif($_SESSION['userlevel'] == 2)
            :	redirect_to("dashboard/index.php");

        else:
            if($_SESSION['userlevel'] == 3)
                :	redirect_to("dashboard/dash_driver.php");
            endif;
        endif;
endif;

?>
<script src="<?php echo BASE_URL?>/assets/assets/js/global.js"></script>
<!-- .page-title start -->
<div class="page-title-style01 page-title-negative-top pt-bkg08" style="padding-top: 255px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Forgot Password</h1>

                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>You are here:</li>
                        <li>
                            <a href="<?php echo BASE_URL;?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL;?>/forgot-password.php">Forgot Password</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-title-style01.page-title-negative-top end -->

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-6">
                <div class="login_page bg-white shadow rounded p-4">
                    <div class="text-center">
                        <h4 class="mb-4"><?php echo $lang['left172'] ?></h4>
                    </div>
                    <?php include("div_loader.php");?>
                    <div id="msgholder2" style="color: red;"><?php print Filter::$showMsg;?></div>

                    <form class="login-form" id="admin_form" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group position-relative">
                                    <label><?php echo $lang['left173'] ?> <span class="text-danger">*</span></label>
                                    <i class="mdi mdi-account ml-3 icons"></i>
                                    <input type="text" class="form-control pl-5" placeholder="<?php echo $lang['left174'] ?>" name="uname" required="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group position-relative">
                                    <label><?php echo $lang['left175'] ?> <span class="text-danger">*</span></label>
                                    <i class="mdi mdi-mail-ru ml-3 icons"></i>
                                    <input type="email" class="form-control pl-5" placeholder="<?php echo $lang['left176'] ?>" name="email" required="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo $lang['left177'] ?><span class="text-danger">*</span></label>
                                    <span class="badge-light"><img src="lib/captcha.php" alt="" class="captcha-append" /></span>
                                </div>
                            </div> <!-- /.col- -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo $lang['left178'] ?> <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="captcha" placeholder="<?php echo $lang['left179'] ?>" required="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <button type="submit" name="dosubmit" style="color: white;padding: 2%;"  class="btn btn-primary rounded w-100"><?php echo $lang['langs_010108'] ?></button>
                            </div>
                        </div>
                    </form>
                    <?php echo Core::doForm("passReset","ajax/user.php");?>
                    <br><br>
                    <p>
                        <?php echo $lang['langs_010109'] ?> </br><?php if($core->reg_allowed):?><a href="sign-up.php" style="color: #006db7;" class="text-primary"><?php echo $lang['langs_010110'] ?></a><?php endif;?> | <a href="index.php" style="color: #006db7;" class="text-primary"><?php echo $lang['langs_010111'] ?></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include "includes/footer.php";
?>
<script>
    function showLoader() {
        $("#loader").fadeIn(200);
    }
    function hideLoader() {
        $("#loader").fadeOut(200);
    };
</script>
