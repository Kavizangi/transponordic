<?php
include "includes/header.php";
require_once "lib/class_mailer.php";

if (isset($_POST['submit_form']) && trim($_POST['submit_form'])=="send"){

    $enquiry = $_POST['enquiry'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $message = $_POST['message'];

    if ( empty($_POST['captcha']) || ( !empty($_POST['captcha']) && $_SESSION['captchacode'] != $_POST['captcha'] ) ){
        $msg_error = "Enter the correct captcha code";
    }else {

        $new_body = "Name: " . $first_name . " " . $last_name . "<br/>";
        $new_body .= "Email: " . $email . "<br/>";
        $new_body .= "Your request: " . $enquiry . "<br/>";
        $new_body .= "Message: " . $message;

        $mailer = $mail->sendMail();
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $message = Swift_Message::newInstance()
            ->setSubject("New Message From Contact Us Form")
            ->setTo(array($core->site_email => 'Admin'))
            ->setFrom(array($email => Registry::get("Core")->site_name))
            ->setBody($new_body, 'text/html');

        if ($mailer->send($message)) {
            $msg_error ="Request sent successfully!";
        }else{
            $msg_error = "Could not send the request, please try again. ".$logger->dump();;
        }

    }

}
?>
<!-- .page-title start -->
<div class="page-title-style01 page-title-negative-top pt-bkg08" style="padding-top: 255px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Contact us</h1>

                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>You are here:</li>
                        <li>
                            <a href="<?php echo BASE_URL;?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL;?>/contact.php">Contact us</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-title-style01.page-title-negative-top end -->

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="custom-heading">
                    <h3>your inquiry</h3>
                </div><!-- .custom-heading.left end -->

                <p>
                    Our team is happy to answer your sales questions. Fill out the form and we’ll be in touch as soon as possible.
                </p>

                <br />

                <!-- .contact form start -->
                <form class="wpcf7 clearfix" action="contact.php" method="post">
                    <div class="row" style="margin-bottom: 0;text-align: center;">
                        <?php
                            if (!empty($msg_error)){
                        ?>
                            <p style="color:red"> <?php echo $msg_error;?></p>
                        <?php
                            }
                        ?>
                    </div>
                    <fieldset>
                        <label>
                            <span class="required">*</span> Your request:
                        </label>

                        <select class="wpcf7-form-control-wrap wpcf7-select" id="contact-inquiry" name="enquiry" required>
                            <option value="Door to door service to Africa (LCL)">Door to door service to Africa (LCL)</option>
                            <option value="Online purchases forwarding service (Consolidated goods)">Online purchases forwarding service (Consolidated goods)</option>
                            <option value="Container request">Container request</option>
                            <option value="Air cargo">Air cargo</option>
                        </select>
                    </fieldset>

                    <fieldset>
                        <label>
                            <span class="required">*</span> First Name:
                        </label>

                        <input type="text" class="wpcf7-text" id="contact-name"  name="first_name" required/>
                    </fieldset>

                    <fieldset>
                        <label>
                            <span class="required">*</span> Last Name:
                        </label>

                        <input type="text" class="wpcf7-text" id="contact-last-name" name="last_name" required/>
                    </fieldset>

                    <fieldset>
                        <label>
                            <span class="required">*</span> Email:
                        </label>

                        <input type="email" class="wpcf7-text" id="contact-email" name="email" required/>
                    </fieldset>

                    <fieldset>
                        <label>
                            <span class="required">*</span> Message:
                        </label>

                        <textarea rows="8" class="wpcf7-textarea" id="contact-message" name="message" required></textarea>
                    </fieldset>

                    <fieldset>
                        <label><?php echo $lang['left177'] ?><span class="text-danger">*</span></label>
                        <span class="badge-light"><img src="lib/captcha.php" alt="" class="captcha-append" /></span>
                    <fieldset>

                    <fieldset>
                        <label><?php echo $lang['left178'] ?> <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="captcha" placeholder="<?php echo $lang['left179'] ?>" required="">
                        <br/>
                    </fieldset>

                    <input type="submit" class="wpcf7-submit" name="submit_form" value="send" />
                </form><!-- .wpcf7 end -->
            </div><!-- .col-md-6 end -->

            <div class="col-md-6">
                <div class="custom-heading">
                    <h3><?php echo ucwords($core->site_name);?> headquarters</h3>
                </div>

                <div id="row">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2272025.1192332953!2d9.300198330285932!3d56.21285473351755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x464b27b6ee945ffb%3A0x528743d0c3e092cd!2sDenmark!5e0!3m2!1sen!2ske!4v1599311981517!5m2!1sen!2ske" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>

                <div class="custom-heading">
                    <h4>Company information</h4>
                </div><!-- .custom-heading end -->

                <address>
                    <?php echo $core->c_address;?>, <br />
                    <?php echo $core->c_city;?>, <?php echo $core->c_country;?>
                </address>

                <span class="text-big colored">
                    <?php echo $core->cell_phone;?>
                </span>
                <br />

                <a href="mailto:<?php echo $core->site_email;?>"><?php echo $core->site_email;?></a>
            </div><!-- .col-md-6 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div>

<?php
include "includes/footer.php";
?>
