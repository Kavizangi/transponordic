<?php
include "includes/header.php";
?>

<!-- .page-title start -->
<div class="page-title-style01 page-title-negative-top pt-bkg08" style="padding-top: 255px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>About TranspoNordic Logistics (TNL)</h1>

                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>You are here:</li>

                        <li>
                            <a href="<?php echo BASE_URL;?>">Home</a>
                        </li>

                        <li>
                            <a href="#">About us</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div>

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="custom-heading">
                    <h2>TranspoNordic Logistics (TNL) | Service to Humanity </h2>
                </div><!-- .custom-heading end -->

                <p>
                    TranspoNordic Logistics (TNL) is an Africa focused courier company that assists private individuals and transport companies  in the movement of their goods to and from Africa.
                    We have specialized services in car and container shipments, forwarding of goods purchased online to their respective buyers, and household and personal effects goods to loved ones in Africa. With our partners across Africa and Europe, we have unparallel delivery service to serve Africa.
                </p>

                <p>
                    TNL is MORE THAN JUST A LOGISTICS AND TRUCKING COMPANY.  We strive to move and deliver your goods safely and securely.  We take extra precautions to ensure that your goods are protected during the storage in our warehouse, transporting over the sea and the goods final journey to your doorstep.
                </p>
            </div><!-- .col-md-6 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content end -->

<div class="page-content custom-bkg bkg-light-blue mb-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="custom-heading">
                    <h2>our mission</h2>
                </div><!-- .custom-heading end -->

                <p>
                    To be the leading global logistic technology provider focussed on delivering reliable logistic services at competitive rates.
                </p>
            </div><!-- .col-md-6 end -->

            <div class="col-md-6">
                <div class="custom-heading">
                    <h2>our services</h2>
                </div><!-- .custom-heading end -->

                <ul class="fa-ul">
                    <li>
                        <i class="fa fa-li fa-long-arrow-right"></i>
                        <a href="services.php?t=sole">Full Container Load - Sole Use Service</a>
                    </li>

                    <li>
                        <i class="fa fa-li fa-long-arrow-right"></i>
                        <a href="services.php?t=shared">Less Than Container Load - Shared Space Service</a>
                    </li>

                    <li>
                        <i class="fa fa-li fa-long-arrow-right"></i>
                        <a href="services.php?t=forwarding"> Forwarding and Consolidation Service</a>
                    </li>

                </ul><!-- .fa-ul end -->
            </div><!-- .col-md-6 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .page-content.custom-bkg end -->

<?php
include "includes/footer.php";
?>
<script>
    /* <![CDATA[ */
    jQuery(document).ready(function ($) {
        'use strict';

        function equalHeight() {
            $('.page-content.column-img-bkg *[class*="custom-col-padding"]').each(function () {
                var maxHeight = $(this).outerHeight();
                $('.page-content.column-img-bkg *[class*="img-bkg"]').height(maxHeight);
            });
        };

        $(document).ready(equalHeight);
        $(window).resize(equalHeight);

        $('#client-carousel').owlCarousel({
            items: 6,
            loop: true,
            margin: 30,
            responsiveClass: true,
            mouseDrag: true,
            dots: false,
            responsive: {
                0: {
                    items: 2,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    responsiveClass: true
                },
                600: {
                    items: 3,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    responsiveClass: true
                },
                1000: {
                    items: 6,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    responsiveClass: true,
                    mouseDrag: true
                }
            }
        });
    });
    /* ]]> */
</script>
