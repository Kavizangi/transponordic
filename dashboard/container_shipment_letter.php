<?php


define("_VALID_PHP", true);
require_once("../init.php");

if (!$user->is_Admin())
    redirect_to("login.php");

$row = Core::getRowById(Core::contaTable, Filter::$id);

?>
<!DOCTYPE html>
<html dir="ltr" lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../uploads/favicon.png">

    <title>Undertaken Letter</title>
    <!-- This page plugin CSS -->

    <!-- Custom CSS -->
    <link href="dist/css/mdb/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/shipment_letter.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid" id="invoice-container">

    <table>
        <tbody>
            <tr class="bordered-bottom-tr">
                <td class="half-width">
                    <?php echo ($core->logo) ? '<img class="logo" src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->site_name.'" />': $core->site_name;?>
                </td>
                <td class="half-width header-contact">
                    <p>Palthomtrresserne 58F</p>
                    <p>3520, Farum, Denmark</p>
                    <p>Tel: 26655068</p>
                    <p>Email: info@tranponordic.dk</p>
                    <p>Website: tranponordic.dk</p>
                    <p>CVR: 38262351</p>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <br/>
    <div class="full-width">
        <h3><?php echo $row->expiration_date; ?></h3>
        <p>Dear <?php echo $row->r_name;?></p><br/>
        <p>Kindly consider this letter as my undertaking to take responsibility for the container shipment you inspected with below information.</p>
        <br/>
        <br/>
        <p>Container number:​ <strong><?php echo $row->container_no; ?></strong></p>
        <p>Place of loading/Departure/Origin Port: <?php echo $row->origin_port;?> </p>
        <p>Destination Port : <?php echo $row->destination_port;?></p>
        <br/>
        <p>Thank you.</p>
        <br/>
        <p>Kind regards</p>
        <br/>
        <br/>
        <p>.......................</p>
        <p>Emmanuel Yaafi</p>
    </div>

</div>
<script>
    printDiv("invoice-container");
    function printDiv(divName) {

        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = '<link rel="stylesheet" href="assets/css/shipment_letter.css"/><link rel="stylesheet" href="dist/css/mdb/bootstrap.min.css"/>'+printContents;

        window.print();

        document.body.innerHTML = originalContents;

        setTimeout(function () {
            window.close();
        },2000);
    }
</script>
</body>
</html>
