<?php

if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');
?>

<?php include 'templates/head_user.php'; ?>

<div class="row">
    <!-- Column -->
    <?php if($roww->userlevel == 9){?>
        <div class="col-lg-12 col-xl-12 col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">

                        <table id="zero_config" class="table table-condensed table-hover table-striped">
                            <thead>
                            <tr>
                                <th><b>Ref No.</b></th>
                                <th class="text-center"><b>Customer Name</b></th>
                                <th class="text-center"><b>Dated</b></th>
                                <th class="text-center"><b>Action</b></th>
                            </tr>
                            </thead>
                            <div class="m-t-40">
                                <div class="d-flex">
                                    <div class="mr-auto">
                                        <div class="form-group">
                                            <a href="create_loading_customer_list.php"><button type="button" class="btn btn-primary btn"><i class="ti-plus" aria-hidden="true"></i> Create Loading List</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tbody id="projects-tbl">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <?php }else if($roww->userlevel == 2){?>

        <div class="col-lg-12 col-xl-12 col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">

                        <table id="zero_config" class="table table-condensed table-hover table-striped">
                            <thead>
                            <tr>
                                <th><b>Ref No.</b></th>
                                <th class="text-center"><b>Customer Name</b></th>
                                <th class="text-center"><b>Dated</b></th>
                                <th class="text-center"><b>Action</b></th>
                            </tr>
                            </thead>
                            <div class="m-t-40">
                                <div class="d-flex">
                                    <div class="mr-auto">
                                        <div class="form-group">
                                            <a href="loading.php?do=create_loading_list"><button type="button" class="btn btn-primary btn"><i class="ti-plus" aria-hidden="true"></i> Create Loading List</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tbody id="projects-tbl">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- Column -->
</div>
