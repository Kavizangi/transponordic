<?php

  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

$sql = "SELECT * FROM add_courier WHERE username='$username' ORDER BY id DESC";

$requests = $db->fetch_all($sql);
?>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>


	<style type="text/css">
		canvas{
			margin:auto;
		}
		.alert{
			margin-top:20px;
		}
	</style>

<?php include 'templates/head_tab.php'; ?>


<div class="row">
	<!-- Column -->
	<div class="col-sm-12 col-lg-4">
		<div class="card">
			<div class="card-body">
				<div class="col-md-12 ">
					<p><?php echo $lang['left1000'] ?> <b><?php echo $row->fname;?> <?php echo $row->lname;?></b></p>
				</div>
			</div>	
		</div>
	</div>
	<!-- Column -->
	<div class="col-sm-12 col-lg-8">
		<div class="card">
			<div class="card-body">
				<div class="row m-t-10">
					<!-- col -->
					<div class="col-md-6 col-sm-12 col-lg-6">
						<div class="d-flex align-items-center">
							<div class="m-r-10"><span class="text-orange display-5"><i class="mdi mdi-cube-send" style="color:#f62d51"></i></span></div>
							<div><span class="text-muted"><?php echo 'Pickup Requests'; ?></span>
								<h3 class="font-medium m-b-0">
                                    <?php echo count($requests); ?>
                                </h3>
							</div>
						</div>
					</div>
					<!-- col -->
					<!-- col -->
					<div class="col-md-6 col-sm-12 col-lg-6">
						<div class="d-flex align-items-center">
							<div class="m-r-10"><span class="text-primary display-5"><i class="mdi mdi-credit-card"></i></span></div>
							<div><span class="text-muted"><?php echo 'Payments'; ?></span>
								<h3 class="font-medium m-b-0"> 
                                    <?php
                                        echo !empty($payments) ? count($payments) : 0;
                                    ?>
                                </h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Sales chart -->
<!-- ============================================================== -->
<div class="row">	
	<div class="col-lg-12">
	
		<div class="card">
			<div class="card-body">
				<div class="row">
					<!-- column -->
					<div class="d-md-flex align-items-center">
						<div><h4 class="card-title"><?php echo 'Door to Door Services' ?></h4></div>
					</div>
					<!-- column -->
					<div id="msgholder"></div>
                    <div class="table-responsive">
                        <table id="zero_config" cellpadding="0" cellspacing="0" border="0" class="table table-striped" >
                            <thead>
                            <tr>
                                <th><b><?php echo '#' ?></b></th>
                                <th><b><?php echo 'Tracking' ?></b></th>
                                <th class="text-center"><b><?php echo 'Courier'; ?></b></th>
                                <th class="text-center"><b><?php echo 'Collection Time'; ?></b></th>
                                <th class="text-center"><b><?php echo 'Delivery Time'; ?></b></th>
                                <th class="text-center"><b><?php echo 'Cost' ?></b></th>
                                <th class="text-center"><b><?php echo 'Payment Status' ?></b></th>
                                <th class="text-center"><b><?php echo 'Courier Status' ?></b></th>
                                <th class="text-center"><b><?php echo 'Pickup Status' ?></b></th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;
                            if (!empty($requests)) {
                                foreach ($requests as $request) {
                            ?>
                                    <tr>
                                        <td><b><?php echo $i++; ?></b></td>
                                        <th><b><?php echo $request->letter_or.$request->tracking; ?></b></th>
                                        <th class="text-center"><b><?php echo $request->courier; ?></b></th>
                                        <th class="text-center"><b><?php echo $request->collection_courier; ?></b></th>
                                        <th class="text-center"><b><?php echo $request->deli_time; ?></b></th>
                                        <th class="text-center"><b><?php echo $request->r_curren.' '.$request->r_costtotal; ?></b></th>
                                        <th class="text-center"><b><?php echo $request->payment_status==1?'paid':'unpaid'; ?></b></th>
                                        <th class="text-center"><b><?php echo $request->status_courier; ?></b></th>
                                        <th class="text-center"><b><?php echo $request->status_pickup==1?'done':'pending'; ?></b></th>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    Action
                                                </button>
                                                <div class="dropdown-menu">
                                                    <?php
                                                    if ($request->status_pickup != 'done'){
                                                        ?>
                                                        <a class="dropdown-item" href="edit_pickup_client.php?id=<?php echo $request->id;?>">
                                                            <i style="color:#343a40" class="ti-pencil"></i> <?php echo 'Edit' ?>
                                                        </a>
                                                    <?php
                                                    }
                                                    if ($request->payment_status!=1) {
                                                        ?>
                                                        <a class="dropdown-item" href="#">
                                                            <i style="color:#343a40" class="ti-credit-card"></i> <?php echo 'Pay' ?>
                                                        </a>
                                                        <?php
                                                    }
                                                    ?>
                                                    <a class="dropdown-item" data-toggle="modal" data-target="#modalItems<?php echo $request->id;?>" href="#"><i style="color:#343a40" class="ti-list"></i> Items</a>
                                                </div>
                                            </div>

                                            <div id="modalItems<?php echo $request->id;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog modal-lg">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <div class="col-md-12">
                                                                <h4 class="modal-title">Items for <?php echo $request->letter_or.$request->tracking;?></h4>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-condensed table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Description</th>
                                                                            <th>Quantity</th>
                                                                            <th>Weight</th>
                                                                            <!-- <th>L*W*H</th> -->
                                                                            <th>Package Type</th>
                                                                             <th>HS Code</th>
                                                                        </tr>
                                                                        </thead>

                                                                        <tbody>
                                                                        <?php
                                                                        $sql="SELECT * FROM detail_addcourier WHERE detail_addcourier.id_add='".$request->id."'";
                                                                        $items = $db->fetch_all($sql);
                                                                        if (!empty($items)) {
                                                                            foreach ($items as $item) {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?php echo $item->detail_description;?></td>
                                                                                    <td><?php echo $item->detail_qnty;?></td>
                                                                                    <td><?php echo $item->detail_weight;?></td>
                                                                                    <!-- <td><?php echo $item->detail_length; ?></td> -->
                                                                                    <td><?php echo $item->package_type;?></td>
                                                                                    <td><?php echo $item->hs_code;?></td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="app.js"></script>