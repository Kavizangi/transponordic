<ul id="sidebarnav">
    <!-- User Profile-->
    <li>
        <!-- User Profile-->
        <div class="user-profile d-flex no-block dropdown m-t-20">
            <div class="user-pic">
                <img src="../uploads/<?php echo ($row->avatar) ? $row->avatar : "blank.png";?>" class="rounded-circle" width="50" />
            </div>
            <?php
            date_default_timezone_set("".$core->timezone."");
            $t = date("H");

            if($t < 12){
                $mensaje = ''.$lang['message1'].'';
            }
            else if($t < 18){
                $mensaje = ''.$lang['message2'].'';
            }
            else{
                $mensaje = ''.$lang['message3'].'';
            }
            ?>

            <div class="user-content hide-menu m-l-10">
                <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <h5 class="m-b-0 user-name font-medium"><?php echo $mensaje; ?>,&nbsp;&nbsp;</h5>
                    <span class="op-5 user-email"><?php echo $row->fname;?></span>
                    <br><b><?php echo 'Customer';?></b>
                </a>
            </div>
        </div>
        <!-- End User Profile prealert.php -->
    </li>

    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="client.php" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu"> <?php echo $lang['dashboard'] ?> </span></a></li>
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="customer_prealert.php" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu"> <?php echo 'Forwarding Service' ?> </span></a></li>

    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="client_pickup_requests.php" aria-expanded="false"><i class="mdi mdi-clock-fast" style="color:#f62d51"></i><span class="hide-menu"> <?php echo 'Door to Door Services' ?> </span></a></li>

    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="consolidated_list.php" aria-expanded="false"><i class="ti ti-gift"></i><span class="hide-menu"> <?php echo 'Consolidated List'; ?> </span></a></li>
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="" aria-expanded="false"><i class="ti ti-gift"></i><span class="hide-menu"> <?php echo 'Containers'; ?> </span></a></li>
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="booking_list.php" aria-expanded="false"><i class="mdi mdi-cube-send"></i><span class="hide-menu"> <?php echo 'Packaging List' ?> </span></a></li>
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="delivered_booking_list.php" aria-expanded="false"><i class="mdi mdi-package-variant"></i><span class="hide-menu"> <?php echo 'Delivered List' ?> </span></a></li>
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="quote.php" aria-expanded="false"><i class="ti ti-stats-up" style="color:#6610f2"></i><span class="hide-menu"> <?php echo 'Send your Quote' ?> </span></a></li>
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="history_quotes.php" aria-expanded="false"><i class="ti ti-archive"></i><span class="hide-menu"> <?php echo 'Quote History' ?> </span></a></li>

    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="client_payments.php" aria-expanded="false"><i class="mdi mdi-credit-card"></i><span class="hide-menu"> <?php echo 'Payments'; ?> </span></a></li>
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="contact.php" aria-expanded="false"><i class="mdi mdi-contacts"></i><span class="hide-menu"> <?php echo 'Contact'; ?> </span></a></li>

    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="user_client.php" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu"> <?php echo $lang['left43'] ?> </span></a></li>

    <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu"></span></li>

    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="../logout.php" aria-expanded="false"><i class="fa fa-power-off m-r-5 m-l-5"></i><span class="hide-menu"><?php echo $lang['wout'] ?></span></a></li>
</ul>