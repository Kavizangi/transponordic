<?php
define("_VALID_PHP", true);
require_once("../init.php");
if (!$user->logged_in)
    redirect_to("login.php");

$row = $user->getUserData();
$username = $row->username;
$sql="SELECT container_payments.*,add_container.username,add_container.letter_or,add_container.container_no,add_container.booking_no,add_container.tracking,add_container.order_inv
        FROM container_payments LEFT JOIN add_container ON add_container.id = container_payments.container_id WHERE add_container.username = '$username'";
$payments = $db->fetch_all($sql);

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Courier DEPRIXA-Integral Web System" />
    <meta name="author" content="Jaomweb">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../uploads/favicon.png">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <title><?php echo $lang['dashboard'] ?> | <?php echo $core->site_name ?></title>
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">

    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <style type="text/css">
        canvas{
            margin:auto;
        }
        .alert{
            margin-top:20px;
        }
    </style>

</head>

<body>

<div id="main-wrapper">

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <?php include 'preloader.php'; ?>

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <?php include 'topbar.php'; ?>

    <!-- End Topbar header -->


    <!-- Left Sidebar - style you can find in sidebar.scss  -->

    <?php include 'left_sidebar.php'; ?>


    <!-- End Left Sidebar - style you can find in sidebar.scss  -->

    <!-- Page wrapper  -->

    <div class="page-wrapper">

        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title"><?php echo 'Your Payments' ?></h4>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="zero_config" cellpadding="0" cellspacing="0" border="0" class="table table-striped" >
                                <thead>
                                    <tr>
                                        <th><b><?php echo '#' ?></b></th>
                                        <th><b><?php echo 'Tracking' ?></b></th>
                                        <th class="text-center"><b><?php echo 'Container No.'; ?></b></th>
                                        <th class="text-center"><b><?php echo 'Booking No.'; ?></b></th>
                                        <th class="text-center"><b><?php echo 'Amount'; ?></b></th>
                                        <th class="text-center"><b><?php echo 'Date' ?></b></th>
                                        <th class="text-center"><b><?php echo 'Description' ?></b></th>
                                    </tr>
                                </thead>
                                <tbody>

                                <?php $i=1;
                                if (!empty($payments)) {
                                    foreach ($payments as $payment) {
                                ?>
                                        <tr>
                                            <td><b><?php echo $i++; ?></b></td>
                                            <th><b><?php echo $payment->letter_or.$payment->tracking; ?></b></th>
                                            <th class="text-center"><b><?php echo $payment->container_no; ?></b></th>
                                            <th class="text-center"><b><?php echo $payment->booking_no; ?></b></th>
                                            <th class="text-center"><b><?php echo number_format($payment->amount,2); ?></b></th>
                                            <th class="text-center"><b><?php echo $payment->date; ?></b></th>
                                            <th class="text-center"><b><?php echo $payment->description; ?></b></th>
                                        </tr>
                                <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                         </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- footer -->

        <script src="app.js"></script>
        <?php include 'templates/footer.php'; ?>
