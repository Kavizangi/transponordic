<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>

<?php include 'templates/head_user.php'; ?>

<?php $pickuprow = $core->getPrealert_list(); ?>
<?php $pickupemployeerow = $user->getPrealertemployee_list(); ?>
<div class="row">
	<!-- Column -->
	<?php if($roww->userlevel == 9){?>
	<div class="col-lg-12 col-xl-12 col-md-12">
		<div class="card">
			<div class="card-body">			
				<div class="table-responsive">
					<table id="zero_config" class="table table-condensed table-hover table-striped">
						<thead>
							<tr>
								<th><b><?php echo $lang['ltracking'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left46'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['ncustomer'] ?></b></th>
                                <th class="text-center"><b><?php echo 'Weight' ?></b></th>
                                 <th class="text-center"><b><?php echo 'Shipping Cost' ?></b></th>
								<th class="text-center"><b><?php echo $lang['left47'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left48'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left49'] ?></b></th>
								<th class="text-center"><b><?php echo 'PickUp Type' ?></b></th>
								<th class="text-center"><b><?php echo $lang['aaction'] ?></b></th>
							</tr>
						</thead>
						<div class="m-t-40">
							<div class="d-flex">
								<div class="mr-auto">
									<div class="form-group">
										<a href="prealert.php"><button type="button" class="btn btn-primary btn"><i class="ti-plus" aria-hidden="true"></i>  <?php echo $lang['left53'] ?></button></a>
									</div>
								</div>
							</div>
						</div>
						<tbody id="projects-tbl">
							<?php if(!$pickuprow):?>
							<tr>
								<td colspan="10">
								<?php echo "
								<i align='center' class='display-3 text-warning d-block'><img src='assets/images/alert/ohh_shipment.png' width='140' /></i>
								",false;?>
								</td>
							</tr>
							<?php else: ?>
							<?php $displayed=array(); foreach ($pickuprow as $row):?>
							<?php
                              if(in_array($row->id, $displayed)){

                              }else{


							?>
							<tr>
								<td><b><a  href="edit_courier.php?do=edit_courier&amp;action=ship&amp;id=<?php echo $row->id;?>"><?php echo $row->order_inv;?></a></b></td>
								<td><?php echo $row->order_purchase;?></td>
								<td><?php echo $row->s_name;?></td>
								<td><?php echo $row->weight_forward;?></td>
								<td class="text-center"><?php echo number_format($core->getPrealert_sumcost($row->id)); ?>
                                    <br>
                                    <a class="btn btn-info" href="javascript: loadpick_upcost(<?php echo $row->id;  ?>,1)">Change/View Cost Analysis</a></td>
								<td class="text-center"><?php echo $row->courier;?></td>
								<td class="text-center"><?php echo $row->supplier;?></td>
								<td class="text-center"><?php echo $row->r_description;?></td>
								<td class="text-center"><span style="background: <?php echo $row->color; ?>;"  class="label label-large" ><?php echo $row->status_courier;?></span></td>
								<td align='center'>
								<a  href="edit_courier_online.php?do=edit_courier_online&amp;action=ship&amp;id=<?php echo $row->id;?>" ><button type="button" class="btn btn-sm btn-icon btn-success btn-outline"><?php echo $lang['aapprove'] ?></button></a>
								</td>
							</tr>
							<?php array_push($displayed,$row->id); }endforeach;?>
							<?php unset($row);?>
							<?php endif;?>
						</tbody>
					</table>
					<?php echo $pager->display_pages();?>
					<?php echo Core::doDelete("Delete Shipment","deleteCourier");?> 
				</div>
			</div>
		</div>
	</div>
	
	<?php }else if($roww->userlevel == 2){?>
	
	<div class="col-lg-12 col-xl-12 col-md-12">
		<div class="card">
			<div class="card-body">			
				<div class="table-responsive">
					<table id="zero_config" class="table table-condensed table-hover table-striped">
						<thead>
							<tr>
								<th><b><?php echo $lang['ltracking'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left46'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['ncustomer'] ?></b></th>	
								<th class="text-center"><b><?php echo $lang['left47'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left48'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left49'] ?></b></th>
								<th class="th-sm" align='center'><b><?php echo $lang['left50'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left51'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['left52'] ?></b></th>
								<th class="text-center"><b><?php echo $lang['aaction'] ?></b></th>
							</tr>
						</thead>
						<div class="m-t-40">
							<div class="d-flex">
								<div class="mr-auto">
									<div class="form-group">
										<a href="prealert.php"><button type="button" class="btn btn-primary btn"><i class="ti-plus" aria-hidden="true"></i> <?php echo $lang['left53'] ?></button></a>
									</div>
								</div>
							</div>
						</div>
						<tbody id="projects-tbl">
							<?php if(!$pickupemployeerow):?>
							<tr>
								<td colspan="10">
								<?php echo "
								<i align='center' class='display-3 text-warning d-block'><img src='assets/images/alert/ohh_shipment.png' width='140' /></i>
								",false;?>
								</td>
							</tr>
							<?php else: ?>
							<?php foreach ($pickupemployeerow as $row):?>
							<tr>
								<td><b><a  href="edit_courier.php?do=edit_courier&amp;action=ship&amp;id=<?php echo $row->id;?>"><?php echo $row->order_inv;?></a></b></td>
								<td><?php echo $row->order_purchase;?></td>
								<td><?php echo $row->s_name;?></td>
								<td class="text-center"><?php echo $row->courier;?></td>
								<td class="text-center"><?php echo $row->supplier;?></td>
								<td class="text-center"><?php echo $row->r_description;?></td>								
								<td align='center'><a  href="view-google-maps.php?do=view-google-maps&amp;action=mapview&amp;id=<?php echo $row->id;?>" target="_blank"><input  type=image src="assets/images/icon_map.png" ></a></td>
								<td class="text-center"><span style="background: <?php echo $row->color; ?>;"  class="label label-large" ><?php echo $row->status_courier;?></span></td>
								<td class="text-center"><?php echo $row->level;?></td>
								<td align='center'>
								<a  href="edit_courier_online.php?do=edit_courier_online&amp;action=ship&amp;id=<?php echo $row->id;?>" ><button type="button" class="btn btn-sm btn-icon btn-success btn-outline"><?php echo $lang['aapprove'] ?></button></a>
								</td>
							</tr>
							<?php endforeach;?>
							<?php unset($row);?>
							<?php endif;?>
						</tbody>
					</table>
					<?php echo $pager->display_pages();?>
					<?php echo Core::doDelete("Delete Shipment","deleteCourier");?> 
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- Column -->
</div>
      <!-- The Modal -->
  <div class="modal" id="modalcourriercosts" class="modal fade" tabindex="-1" role="dialog" data-backdrop="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <form id='form_save_cost' method="POST" action="assets/add_pickupcost.php"  enctype="multipart/form-data">
        <input type='text' class='form-control' name='prealert' id='prealert'>
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Costing Details</h4>
          
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="costslist">
        
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <div class="row">
                <div class="col-md-6">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style='margin:10px;width:150px; height:50px;'>Close</button>
                </div>
                <div class="col-md-6">
                    <button type='submit' id="savepickupcost_id" class='btn btn-success m-70 p-70 pull-right' style='margin:10px;width:150px; height:50px;'>Save</button> 
                </div>
                
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>

<script src="dist/js/stacktable.js"></script>
<script>
	$('table').stacktable();
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var count_rows=0;
       loadpick_upcost = function(prealert ,userlevel){
        // alert("well");
        if(userlevel==0){
            $("#savepickupcost_id").hide();
        }
        $("#prealert ").val(prealert );
        var url="assets/getprealertcost.php"
        var formData={prealert :prealert };
                     $.ajax({
                            type:'POST',
                            url: url,
                            data: formData,
                            success:function(data){
                                console.log(data);
                            $("#modalcourriercosts").modal("hide");
                                 
                              var parsed=JSON.parse(data);
                              $("#costslist").html("");
                             //    var no =0; 
                             var user_row="<table class='table table-striped table-bordered'><thead><tr><th>#</th><th>Service</th><th>Cost</th><th><a class='btn btn-primary' style='width:100%;' href='javascript: addrow("+count_rows+")'>Add Row</a></th></tr></thead><tbody id='tbody_costs'>"; 

                             $.each(parsed.payments, function(k, v) {
                                
                                   user_row+="<tr id='row_"+count_rows+"'><td><input readonly='readonly' type='text' class='form-control' value='"+v.id+"' name='id[]'></td><td><input type='text' class='form-control' value='"+v.service_desc+"' name='servicedesc[]'></td><td><input type='text' class='form-control' value='"+v.service_cost+"' name='servicecost[]'></td><td><a class='btn btn-danger' style='width:100%;' href='javascript: removerow("+count_rows+")'>Remove Row</a></td></tr>";
                                   count_rows=count_rows+1;
                                    
                                 }); 
                                 user_row+="</tbody></tbody></table>"; 
                                 // $("#modalcourriercosts").append(user_row);
                                  $("#costslist").append(user_row);
                                  $("#modalcourriercosts").modal("show");
                                          
                            },
                            error:function(data){

                                       console.log(data);
                            }        

             });        
       }
       $("#form_save_cost").submit(function(e){
           e.preventDefault();
            var url="assets/add_forwardcost.php"
    //             $.post(url, $.param($("#form_save_cost").serializeArray()), function(data) { // use this ajax code
    //            console.log(data);
    // });
             $.ajax({
                     type:'POST',
                     url :url,
                     data:new FormData(this),
                     contentType : false,
                     processData: false,
                    success: function (data) {
                       // loadpick_upcost($("#prealert").val(),1);
                      console.log(data);
                     },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
       
       addrow=function(){
            $("#tbody_costs").prepend("<tr id='row_"+count_rows+"'><td><input readonly='readonly' type='text' class='form-control' value='' name='id[]'></td><td><input type='text' class='form-control' value='' name='servicedesc[]'></td><td><input type='text' class='form-control'  name='servicecost[]'></td><td><a class='btn btn-danger' style='width:100%;' href='javascript: removerow("+count_rows+")'>Remove Row</a></td></tr>");
            count_rows=count_rows+1;
       }
       removerow=function(row){
          $("#row_"+row).remove();
       }
    });
</script>