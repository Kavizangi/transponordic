<?php

define("_VALID_PHP", true);
require_once("../init.php");

if (!$user->logged_in)
    redirect_to("login.php");

$row = $user->getUserData();
$ratesrow = $core->getRates();

?>
    <!DOCTYPE html>
    <html dir="ltr" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="../uploads/favicon.png">

        <title>Pre Alerts | <?php echo $core->site_name ?></title>
        <!-- This page plugin CSS -->
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/style.min.css" rel="stylesheet">

        <link href="../assets/css_log/front.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="../assets/js/jquery.js"></script>
        <script type="text/javascript" src="../assets/js/jquery-ui.js"></script>
        <script src="../assets/js/jquery.ui.touch-punch.js"></script>
        <script src="../assets/js/jquery.wysiwyg.js"></script>
        <script src="../assets/js/global.js"></script>
        <script src="../assets/js/custom.js"></script>
        <script src="../assets/js/modernizr.mq.js" type="text/javascript" ></script>
        <script src="../assets/js/checkbox.js"></script>
        <script src="../assets/js/menu.js"></script>
        <link rel="StyleSheet" href="dist/css/calculator.css" type="text/css">
        <script src="assets/js/checkbox.js"></script>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>

    </head>

<body>

<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

<?php include 'preloader.php'; ?>

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

<?php include 'topbar.php'; ?>

    <!-- End Topbar header -->


    <!-- Left Sidebar - style you can find in sidebar.scss  -->

<?php include 'left_sidebar.php'; ?>

    <!-- End Left Sidebar - style you can find in sidebar.scss  -->

    <!-- Page wrapper  -->
    <div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Contact Us</h4>
            </div>
        </div>
    </div>
    <?php $courierrow = $core->getCouriercom(); ?>
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 mx-auto text-center">
                                <h2 class="h1">
                                    <br/>
                                    <?php echo 'Contact'; ?>
                                </h2>
                                <div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-40 mx-auto"></div>
                                <p>Feel free to contact our team anytime.</p>
                            </div>
                        </div> <!-- END row-->

                        <div class="row">
                            <div class="text-center col-md-4">
                                <div class="card mt-4">
                                    <div class="card-body">
                                        <i class="mdi mdi-google-maps font-40 m-b-15"></i>
                                        <h6 class="text-uppercase">Office Locations</h6>
                                        <p class="text-muted m-t-20"><?php echo $core->c_address;?>, <br><?php echo $core->c_city." ".$core->c_country;?></p>

                                    </div>
                                </div>
                            </div>
                            <div class="text-center col-md-4">
                                <div class="card mt-4">
                                    <div class="card-body">
                                        <i class=" mdi mdi-phone-classic font-40 m-b-15"></i>
                                        <h6 class="text-uppercase">
                                            Call Us</h6>
                                        <p class="text-muted m-t-20"><?php echo $core->cell_phone;?></p>
                                        <p class="text-muted m-t-20"><?php echo $core->c_phone;?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center col-md-4">
                                <div class="card mt-4">
                                    <div class="card-body">
                                        <i class="mdi mdi-email font-40 m-b-15"></i>
                                        <h6 class="text-uppercase">
                                            Do you need Support?</h6>
                                        <p class="text-muted m-t-20">Enquiries on the use of the
                                            software, and comments should be sent to <a href="mailto:<?php echo $core->site_email;?>" class="text-decoration-underline"><?php echo $core->site_email;?></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr class="u-my-60">
                    </div> <!-- END container-->
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>

    <!-- footer -->

<?php include 'templates/footer.php'; ?>