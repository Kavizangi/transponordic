			<?php
			  
			  if (!defined("_VALID_PHP"))
				  die('Direct access to this location is not allowed.');
			?>			
			
			<footer class="footer text-center">
				&copy; <?php echo date('Y').' '.$core->site_name;?> - <?php echo $lang['foot'] ?>
			</footer>

            <!-- End footer -->
        </div>
    </div>

    <!-- End Wrapper -->

        <!-- ============================================================== -->
		<!-- All Jquery -->
		<!-- ============================================================== -->
		<!-- Bootstrap tether Core JavaScript -->

		<script src="<?php SITEURL ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
		<script src="<?php SITEURL ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- apps -->
		<script src="<?php SITEURL ?>dist/js/app.min.js"></script>
		<script src="<?php SITEURL ?>dist/js/app.init.js"></script>
		<script src="<?php SITEURL ?>dist/js/app-style-switcher.js"></script>
		<!-- slimscrollbar scrollbar JavaScript -->
		<script src="<?php SITEURL ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
		<script src="<?php SITEURL ?>assets/extra-libs/sparkline/sparkline.js"></script>
		<!--Wave Effects -->
		<script src="<?php SITEURL ?>dist/js/waves.js"></script>
		<!--Menu sidebar -->
		<script src="<?php SITEURL ?>dist/js/sidebarmenu.js"></script>
		<!--Custom JavaScript -->
		<script src="<?php SITEURL ?>dist/js/custom.min.js"></script>

		<script type="text/javascript">
		$(function() {
            $("#searchnames").autocomplete({
                source: "search_ship.php",
                minLength: 2,
                select: function(event, ui) {
                    event.preventDefault();
                    $('#searchnames').val(ui.item.fname);
                    $('#idx').val(ui.item.idx);
                    $('#mail').val(ui.item.mail);
                    $('#phones').val(ui.item.phones);
                    $('#citys').val(ui.item.citys);
                    $('#zones').val(ui.item.zones);
                    $('#zips').val(ui.item.zips);

                    $("#searchnames").focus();
                 }
            });
        });
		</script>
		
		<script type="text/javascript">
		// <![CDATA[
		  function showResponse(msg) {
			  hideLoader();
			  if (msg.trim() === 'OK') {
				  result = "<div class=\"bggreen\"><p><span class=\"icon-ok-sign\"><\/span><i class=\"close icon-remove-circle\"></i><span>Success!<\/span>You have successfully registered. Please check your email for further information<\/p><\/div>";
				  $("#fullform").hide();
			  } else {
			      if (msg.trim() === "BOOKED"){
                    result = "<div class='alert alert-success' id='success-alert'><p><span class='icon-ok-sign'></span>" +
                        "<i class='close icon-remove-circle'></i><span> Success! </span> Prior alert added successfully!" +
                        "<br>When the previous alert is approved, it will appear on your dashboard.</p></div>";
                      window.setTimeout(function(){
                          // Move to a new location or you can do something else
                          window.location.href = "<?php echo SITEURL;?>/dashboard/prealert.php";
                      }, 1000);

                  }else {
                    result = msg;
                  }
			  }
			  $(this).html(result);
			  $("html, body").animate({
				  scrollTop: 0
			  }, 600);
		  }
		  $(document).ready(function () {
			  let options = {
				  target: "#msgholder",
				  beforeSubmit: showLoader,
				  success: showResponse,
				  url: "../ajax/user.php",
				  resetForm: 0,
				  clearForm: 0,
				  data: {
					  processContact: 1
				  }
			  };
			  $("#client_form").ajaxForm(options);
		  });
		// ]]>
		</script>
	
	<script type="text/javascript"> 
	// <![CDATA[
	$(document).ready(function () {
		var itemscount=0;
		$('a.activate').on('click', function () {
			let uid = $(this).attr('id').replace('act_', '');
			let text = "<i class=\"icon-warning-sign icon-3x pull-left\"></i>Are you sure you want to activate this user account?<br /><strong>Email notification will be sent as well</strong>";
			new Messi(text, {
				title: "Activate User Account",
				modal: true,
				closeButton: true,
				buttons: [{
					id: 0,
					label: "Activate",
					val: 'Y'
				}],
				  callback: function (val) {
					  $.ajax({
						  type: 'post',
						  url: "controller.php",
						  data: {
							  activateAccount: 1,
							  id: uid,
						  },
						  cache: false,
						  beforeSend: function () {
							  showLoader();
						  },
						  success: function (msg) {
							  hideLoader();
							  $("#msgholder").html(msg);
							  $('html, body').animate({
								  scrollTop: 0
							  }, 600);
						  }
					  });
				  }
			});
		});
		additemrow=function(){
			// alert("additemrow"+itemscount);
			itemscount=itemscount+1;
			$("#items_list_table").prepend('<tr id="item_row_'+itemscount+'"><td><input type="text" class="form-control" name="items_name[]" placeholder="Item name"></td><td><input type="number" class="form-control" name="items_quantity[]" placeholder="Item Quantity"></td><td><input type="number" class="form-control" name="items_cost_value[]" placeholder="Item cost"></td><td><input type="number" class="form-control" name="items_weight[]" placeholder="Item Weight"></td><td><input type="text" class="form-control" name="items_package_type[]" placeholder="Package Type"></td><td><input type="text" class="form-control" name="items_hs_code[]" placeholder="HS Code"></td><td><a href="javascript: removeitemrow('+itemscount+')" class="btn btn-warning"><span class="ti ti-minus"></span></a></td></tr>');

		}
		removeitemrow=function(row){
          $("#item_row_"+row).remove();
		}
	});
	// ]]>
	</script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
	
	<!--Datetimepicker -->
	<?php include 'datetimepicker.php'; ?>
		
</body>
</html>