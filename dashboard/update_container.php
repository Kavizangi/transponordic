<?php
 define("_VALID_PHP", true);
 require_once("../init.php");

if(isset($_POST['detail_container'])){
	
	$detail_container	=	sanitize($_POST['detail_container']);		
	$price				=	sanitize($_POST['price']);
	$detail_qty			=	sanitize($_POST['detail_qty']);				
	$detail_weight		= 	sanitize($_POST['detail_weight']);					
	$package_type		= 	sanitize($_POST['package_type']);
	$hs_code		    = 	sanitize($_POST['hs_code']);
	$total				= 	$detail_qty*$price;
	$id = $_POST['id'];

	//  query to update data 
	
	$db->query("UPDATE detail_container SET detail_container='$detail_container', detail_weight='$detail_weight', detail_qty='$detail_qty', price='$price', tprice='$total',hs_code='$hs_code', package_type='$package_type' WHERE id='$id'");
	if($db){
		echo  alert("<strong><center>The data was updated successfully</center></strong>"); 
	}

}

if (isset($_POST['payment'])){
    $description = sanitize($_POST['description']);
    $date	= sanitize($_POST['date']);
    $amount	= sanitize($_POST['amount']);
    $remarks = sanitize($_POST['remarks']);
    $id	= sanitize($_POST['payment_id']);
    $date_now = date('Y-m-d H:i:s');
    $container_id = sanitize($_POST['container_id']);

    if ($id > 0){
        $db->query("UPDATE container_payments SET description='$description', date='$date', amount='$amount', remarks='$remarks' WHERE id='$id'");
        if($db){
            echo  "The data was updated successfully";exit;
        }
    }else{
        $db->query("INSERT INTO `container_payments`(`container_id`,`description`, `date`, `amount`, `remarks`, `created_on`)
                    VALUES ('$container_id','$description','$date','$amount','$remarks','$date_now')");
        if($db){
            echo  "The data was updated successfully";exit;
        }
    }
}

if (isset($_POST['sailingCharge'])){

    $description = sanitize($_POST['description']);
    $invoice_no	= sanitize($_POST['invoice_no']);
    $amount	= sanitize($_POST['amount']);
    $remarks = sanitize($_POST['remarks']);
    $id = sanitize($_POST['charge_id']);
    $container_id = sanitize($_POST['container_id']);
    $date = date('Y-m-d H:i:s');

    if ($id > 0){
        $sql = "UPDATE container_sailing_charges SET description='$description', invoice_no='$invoice_no', amount='$amount', remarks='$remarks' WHERE id='$id'";
        $db->query($sql);
        if($db){
            echo  "The data was updated successfully"; exit;
        }
    }else{
        $sql = "INSERT INTO `container_sailing_charges`(`container_id`, `description`, `invoice_no`, `amount`, `remarks`, `created_on`)
                    VALUES ('$container_id','$description','$invoice_no','$amount','$remarks','$date')";
        $db->query($sql);
        if($db){
            echo  "The data was updated successfully";exit;
        }
    }

    echo "Failed to updated";
}