<?php
define("_VALID_PHP", true);
require_once("../init.php");
if (!$user->logged_in)
    redirect_to("login.php");

$row = $user->getUserData();

$username = $row->username;

$sql = "SELECT * FROM add_courier WHERE username='$username' ORDER BY id DESC";

$requests = $db->fetch_all($sql);
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords" content="Transponordic" />
    <meta name="author" content="Transponordic" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../uploads/favicon.png" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet" />
    <title><?php echo $lang['dashboard'] ?> | <?php echo $core->site_name ?></title>
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet" />
    <link href="assets/libs/chartist/dist/chartist.min.css" rel="stylesheet" />
    <link href="assets/extra-libs/c3/c3.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" />
    <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <style type="text/css">
        canvas{
            margin:auto;
        }
        .alert{
            margin-top:20px;
        }
    </style>

</head>

<body>

<div id="main-wrapper">

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <?php include 'preloader.php'; ?>

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <?php include 'topbar.php'; ?>
    <!-- End Topbar header -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->

    <?php include 'left_sidebar.php'; ?>
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->

    <!-- Page wrapper  -->
    <div class="page-wrapper">

        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title"><?php echo 'Pickup Requests' ?></h4>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-primary" href="add_pickup_client.php"><i class="mdi mdi-plus" style="color:#f62d51"></i><span class="hide-menu"> <?php echo 'Request Door to Door Service' ?> </span></a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" cellpadding="0" cellspacing="0" border="0" class="table table-striped" >
                                <thead>
                                    <tr>
                                        <th><b><?php echo '#' ?></b></th>
                                        <th><b><?php echo 'Tracking' ?></b></th>
                                        <th class="text-center"><b><?php echo 'Courier'; ?></b></th>
                                        <th class="text-center"><b><?php echo 'Collection Time'; ?></b></th>
                                        <th class="text-center"><b><?php echo 'Delivery Time'; ?></b></th>
                                        <th class="text-center"><b><?php echo 'Cost' ?></b></th>
                                        <th class="text-center"><b><?php echo 'Payment Status' ?></b></th>
                                        <th class="text-center"><b><?php echo 'Collection Status' ?></b></th>
                                        <th class="text-center"><b><?php echo 'Pickup Status' ?></b></th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;
                                if (!empty($requests)) {
                                    foreach ($requests as $request) {

                                        ?>
                                        <tr>
                                            <td><b><?php echo $i++; ?></b></td>
                                            <th><b><?php echo $request->letter_or.$request->tracking; ?></b></th>
                                            <th class="text-center"><b><?php echo $request->courier; ?></b></th>
                                            <th class="text-center"><b><?php echo $request->collection_courier; ?></b></th>
                                            <th class="text-center"><b><?php echo $request->deli_time; ?></b></th>
                                            <th class="text-center"><b><?php echo $request->r_curren.' ';?><?php echo $core->getPickup_sumcost($row->id); ?>
                                    <br>
                                    <a class="btn btn-info" href="javascript: loadpick_upcost(<?php echo $row->id;  ?>,1)">Change/View Cost Analysis</a></b></th>
                                            <th class="text-center"><b><?php echo $request->payment_status==1?'paid':'unpaid'; ?></b></th>
                                            <th class="text-center"><b><?php echo $request->status_courier; ?></b></th>
                                            <th class="text-center"><b><?php echo $request->status_pickup==1?'done':'pending'; ?></b></th>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Action
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <?php
                                                            if ($request->status_pickup != 'done'){
                                                        ?>
                                                            <a class="dropdown-item" href="edit_pickup_client.php?id=<?php echo $request->id;?>">
                                                                <i style="color:#343a40" class="ti-pencil"></i> <?php echo 'Edit' ?>
                                                            </a>
                                                        <?php
                                                            }
                                                            if ($request->payment_status!=1) {
                                                        ?>
                                                            <a class="dropdown-item" href="#">
                                                                <i style="color:#343a40" class="ti-credit-card"></i> <?php echo 'Pay' ?>
                                                            </a>
                                                        <?php
                                                            }
                                                        ?>
                                                        <a class="dropdown-item" data-toggle="modal" data-target="#modalItems<?php echo $request->id;?>" href="#"><i style="color:#343a40" class="ti-list"></i> Items</a>
                                                    </div>
                                                </div>

                                                <div id="modalItems<?php echo $request->id;?>" class="modal fade" role="dialog">
                                                    <div class="modal-dialog modal-lg">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <div class="col-md-12">
                                                                    <h4 class="modal-title">Items for <?php echo $request->letter_or.$request->tracking;?></h4>
                                                                </div>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <table class="table table-condensed table-bordered">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Description</th>
                                                                                    <th>Quantity</th>
                                                                                    <th>Weight</th>
                                                                                    <th>L*W*H</th>
                                                                                    <th>Weight Vol</th>
                                                                                </tr>
                                                                            </thead>

                                                                            <tbody>
                                                                                <?php
                                                                                    $sql="SELECT * FROM detail_addcourier WHERE detail_addcourier.id_add='".$request->id."'";
                                                                                    $items = $db->fetch_all($sql);
                                                                                    if (!empty($items)) {
                                                                                        foreach ($items as $item) {
                                                                                ?>
                                                                                            <tr>
                                                                                                <td><?php echo $item->detail_description;?></td>
                                                                                                <td><?php echo $item->detail_qnty;?></td>
                                                                                                <td><?php echo $item->detail_weight;?></td>
                                                                                                <td><?php echo $item->detail_length.'*'.$item->detail_width.'*'.$item->detail_height;?></td>
                                                                                                <td><?php echo $item->detail_vol;?></td>
                                                                                            </tr>
                                                                                <?php
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </tbody>

                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- footer -->

        <script src="app.js"></script>
        <?php include 'templates/footer.php'; ?>
