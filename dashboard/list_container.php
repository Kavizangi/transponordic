<?php

if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');
?>

<?php include 'templates/head_user.php'; ?>


<?php $linerow = $core->getContainerline();?>
<?php $lineemployeerow = $user->getContainerlineemployee();?>
<div class="row">
    <!-- Column -->
    <?php if($roww->userlevel == 9){?>
        <div class="col-lg-12 col-xl-12 col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">

                        <table id="zero_config" class="table table-condensed table-hover table-striped">
                            <thead>
                            <tr>
                                <th><b><?php echo $lang['conlist-title2'] ?></b></th>
                                <th class="text-center"><b>Container No.</b></th>
                                <th class="text-center"><b>Booking No.</b></th>
                                <th class="text-center"><b><?php echo "Customer" ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title4'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title5'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title6'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title7'] ?></b></th>
                                <th class="text-center"><b><?php echo "Status" ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title9'] ?></b></th>
                            </tr>
                            </thead>
                            <div class="m-t-40">
                                <div class="d-flex">
                                    <div class="mr-auto">
                                        <div class="form-group">
                                            <a href="client_container.php"><button type="button" class="btn btn-primary btn"><i class="ti-plus" aria-hidden="true"></i> <?php echo $lang['conlist-title1'] ?></button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tbody id="projects-tbl">
                            <?php if(!$linerow):?>
                                <tr>
                                    <td colspan="10">
                                        <?php echo "
								<i align='center' class='display-3 text-warning d-block'><img src='assets/images/alert/ohh_shipment.png' width='130' /></i>								
								",false;?>
                                    </td>
                                </tr>
                            <?php else:?>
                                <?php foreach ($linerow as $row):?>
                                    <tr>
                                        <td><a href="edit_container.php?do=edit_container&amp;action=edit&amp;id=<?php echo $row->id;?>" ><?php echo $row->order_inv;?></a></td>
                                        <td><?php echo $row->container_no;?></td>
                                        <td><?php echo $row->booking_no;?></td>
                                        <td><?php echo $row->r_name;?></td>
                                        <td class="text-center"><?php echo $row->origin_port;?></td>
                                        <td class="text-center"><?php echo $row->destination_port;?></td>
                                        <td class="text-center"><?php echo $row->courier;?></td>
                                        <td class="text-center">
                                            <?php if ($row->payment_status == 0){ ?>
                                                <?php echo $lang['conlist-title13'] ?>
                                            <?php }else{ ?>
                                                <img src='assets/images/alert/paid.png' width='68' />
                                            <?php } ?>
                                        </td>
                                        <td class="text-center"><span style="background: <?php echo $row->color; ?>;"  class="label label-large" ><?php echo $row->status_courier;?></span></td>
                                        <td align='center'>

                                            <div class="dropdown">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    Action
                                                </button>
                                                <div class="dropdown-menu">

                                                    <a class="dropdown-item" href="edit_container.php?do=edit_container&amp;action=edit&amp;id=<?php echo $row->id;?>"><i style="color:#343a40" class="ti-pencil"></i> <?php echo $lang['tooledit'] ?></a>
                                                    <a class="dropdown-item" data-toggle="modal" data-target="#modalSailingCharges<?php echo $row->id;?>" href="#"><i style="color:#343a40" class="ti-money"></i> Sailing Charges</a>
                                                    <a class="dropdown-item" data-toggle="modal" data-target="#modalPayments<?php echo $row->id;?>" href="#"><i style="color:#343a40" class="ti-credit-card"></i> Payments</a>
                                                    <a class="dropdown-item" href="invoice/commercial_invoice.php?id=<?php echo $row->id;?>" target="_blank"><i style="color:#343a40" class="ti-printer"></i> Commercial Invoice</a>
                                                    <a class="dropdown-item" href="invoice/container.php?do=container&amp;action=ship&amp;id=<?php echo $row->id;?>" target="_blank"><i style="color:#343a40" class="ti-printer"></i> Loading List</a>
                                                    <a class="dropdown-item" href="container_shipment_letter.php?id=<?php echo $row->id;?>" target="_blank"><i style="color:#343a40" class="ti-printer"></i> UnderTaken Letter</a>
                                                    <a class="dropdown-item" href="status_container.php?do=status_container&amp;action=status_container&amp;id=<?php echo $row->id;?>"><i style="color:#20c997" class="ti-reload"></i> <?php echo $lang['toolupdate'] ?></a>
                                                    <a class="dropdown-item" id="item_<?php echo $row->idcon;?>" data-rel="<?php echo $row->username;?>" class="delete"><i style="color:#343a40" class="ti-trash"></i> <?php echo $lang['tooldelete'] ?></a>

                                                </div>
                                            </div>

                                            <!-- Modal -->
                                            <div id="modalPayments<?php echo $row->id;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog modal-lg">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <div class="col-md-12">
                                                                <h4 class="modal-title">Payments for <?php echo $row->order_inv;?></h4><br/>
                                                                <a href="#" onclick="addPayment(<?php echo $row->id;?>)" class="btn btn-primary pull-left">Add</a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-condensed table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Date</th>
                                                                            <th>Description</th>
                                                                            <th>Amount</th>
                                                                            <th>Remarks</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                        </thead>

                                                                        <tbody class="payments_tbody">
                                                                            <?php
                                                                            $total=0;
                                                                                $c_results = $db->fetch_all("SELECT * FROM `container_payments` WHERE container_id='$row->id'");
                                                                                foreach ( $c_results as $c_result){
                                                                                    $c_result = (object) $c_result;
                                                                                    $total += $c_result->amount;
                                                                            ?>
                                                                                <tr>
                                                                                    <td><?php echo $c_result->date;?></td>
                                                                                    <td><?php echo $c_result->description;?></td>
                                                                                    <td><?php echo $c_result->amount;?></td>
                                                                                    <td><?php echo $c_result->remarks;?></td>
                                                                                    <td>
                                                                                        <a href="#" onclick="editPayment(<?php echo $c_result->id;?>)" class="btn btn-info">Edit</a>
                                                                                        <a href="#" onclick="deletePayment(<?php echo $c_result->id.','.$c_result->container_id;?>)" class="btn btn-danger">Delete</a>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                            <tr>
                                                                                <td>Total payment:</td>
                                                                                <td></td>
                                                                                <td><?php echo $total;?></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </tbody>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <!-- Modal -->
                                            <div id="modalSailingCharges<?php echo $row->id;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog modal-lg">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <div class="col-md-12">
                                                                <h4 class="modal-title">Sailing Charges for <?php echo $row->order_inv;?></h4><br/>
                                                                <button class="btn btn-primary pull-left" onclick="addSailingCharge(<?php echo $row->id;?>)">Add</button>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-condensed table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Description</th>
                                                                            <th>Invoice No</th>
                                                                            <th>Amount</th>
                                                                            <th>Remarks</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="sailing_charges_tbody">
                                                                            <?php
                                                                                $c_results = $db->fetch_all("SELECT * FROM `container_sailing_charges` WHERE container_id='$row->id'");
                                                                                foreach ( $c_results as $c_result){
                                                                                    $c_result = (object) $c_result;
                                                                            ?>
                                                                                <tr>
                                                                                    <td><?php echo $c_result->description;?></td>
                                                                                    <td><?php echo $c_result->invoice_no;?></td>
                                                                                    <td><?php echo $c_result->amount;?></td>
                                                                                    <td><?php echo $c_result->remarks;?></td>
                                                                                    <td>
                                                                                        <a href="#" onclick="editSailingCharge(<?php echo $c_result->id;?>)" class="btn btn-info">Edit</a>
                                                                                        <a href="#" onclick="deleteSailingCharge(<?php echo $c_result->id.','.$c_result->container_id;?>)" class="btn btn-danger">Delete</a>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach;?>
                                <?php unset($row);?>
                            <?php endif;?>
                            </tbody>
                        </table>
                        <?php echo $pager->display_pages();?>
                        <?php echo Core::doDelete("Delete Item Container","deleteListcontainer");?>
                    </div>
                </div>
            </div>
        </div>

    <?php }else if($roww->userlevel == 2){?>

        <div class="col-lg-12 col-xl-12 col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">

                        <table id="zero_config" class="table table-condensed table-hover table-striped">
                            <thead>
                            <tr>
                                <th><b><?php echo $lang['conlist-title2'] ?></b></th>
                                <th class="text-center"><b>Container No.</b></th>
                                <th class="text-center"><b>Booking No.</b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title3'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title4'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title5'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title6'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title7'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title8'] ?></b></th>
                                <th class="text-center"><b><?php echo $lang['conlist-title9'] ?></b></th>
                            </tr>
                            </thead>
                            <div class="m-t-40">
                                <div class="d-flex">
                                    <div class="mr-auto">
                                        <div class="form-group">
                                            <a href="client_container.php"><button type="button" class="btn btn-primary btn"><i class="ti-plus" aria-hidden="true"></i> <?php echo $lang['conlist-title1'] ?></button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tbody id="projects-tbl">
                            <?php if(!$lineemployeerow):?>
                                <tr>
                                    <td colspan="10">
                                        <?php echo "
								<i align='center' class='display-3 text-warning d-block'><img src='assets/images/alert/ohh_shipment.png' width='130' /></i>								
								",false;?>
                                    </td>
                                </tr>
                            <?php else:?>
                                <?php foreach ($lineemployeerow as $row):?>
                                    <tr>
                                        <td><a href="edit_container.php?do=edit_container&amp;action=edit&amp;id=<?php echo $row->id;?>" ><?php echo $row->order_inv;?></a></td>
                                        <td><?php echo $row->container_no;?></td>
                                        <td><?php echo $row->booking_no;?></td>
                                        <td><?php echo $row->r_name;?></td>
                                        <td class="text-center"><?php echo $row->origin_port;?></td>
                                        <td class="text-center"><?php echo $row->destination_port;?></td>
                                        <td class="text-center"><?php echo $row->courier;?></td>
                                        <td class="text-center">
                                            <?php if ($row->payment_status == 0){ ?>
                                                <?php echo $lang['conlist-title13'] ?>
                                            <?php }else{ ?>
                                                <img src='assets/images/alert/paid.png' width='68' />
                                            <?php } ?>
                                        </td>
                                        <td class="text-center"><span style="background: <?php echo $row->color; ?>;"  class="label label-large" ><?php echo $row->status_courier;?></span></td>
                                        <td align='center'>
                                            <div class="dropdown">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    Action
                                                </button>
                                                <div class="dropdown-menu">

                                                    <a class="dropdown-item" href="edit_container.php?do=edit_container&amp;action=edit&amp;id=<?php echo $row->id;?>"><i style="color:#343a40" class="ti-pencil"></i> <?php echo $lang['tooledit'] ?></a>
                                                    <a class="dropdown-item" href="invoice/commercial_invoice.php?id=<?php echo $row->id;?>" target="_blank"><i style="color:#343a40" class="ti-printer"></i> Commercial Invoice</a>
                                                    <a class="dropdown-item" href="invoice/container.php?do=container&amp;action=ship&amp;id=<?php echo $row->id;?>" target="_blank"><i style="color:#343a40" class="ti-printer"></i> Loading List</a>
                                                    <a class="dropdown-item" href="container_shipment_letter.php?id=<?php echo $row->id;?>" target="_blank"><i style="color:#343a40" class="ti-printer"></i> Print UnderTaken Letter</a>
                                                    <a class="dropdown-item" href="status_container.php?do=status_container&amp;action=status_container&amp;id=<?php echo $row->id;?>"><i style="color:#20c997" class="ti-reload"></i> <?php echo $lang['toolupdate'] ?></a>
                                                    <a class="dropdown-item" id="item_<?php echo $row->idcon;?>" data-rel="<?php echo $row->username;?>" class="delete"><i style="color:#343a40" class="ti-trash"></i> <?php echo $lang['tooldelete'] ?></a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                <?php unset($row);?>
                            <?php endif;?>
                            </tbody>
                        </table>
                        <?php echo $pager->display_pages();?>
                        <?php echo Core::doDelete("Delete Item Container","deleteListcontainer");?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- Column -->
</div>

<!-- Modal -->
<div id="addPayment" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Payment</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="addPaymentForm">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea id="description" name="description" class="form-control" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input class="form-control" type="date" name="date" id="date"/>
                            </div>
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input class="form-control" type="number" name="amount" id="amount"/>
                                <input name="id" type="hidden" value="0"/>
                                <input name="container_id" class="container_id" type="hidden" value="0"/>
                                <input name="payment" type="hidden" value="0"/>
                            </div>
                            <div class="form-group">
                                <label for="remarks">Remarks</label>
                                <textarea id="remarks" name="remarks" class="form-control" rows="4"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary addPayment">Submit</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="addSailingCharge" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p class="response"></p><br/>
                <form id="sailingChargeForm">
                    <div class="form-group">
                        <label for="description">Description</label>s
                        <textarea id="description" name="description" class="form-control" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="invoice_no">Invoice No.</label>
                        <input id="invoice_no" name="invoice_no" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input class="form-control" name="amount" type="number" id="amount"/>
                        <input name="id" type="hidden" value="0"/>
                        <input name="container_id" class="container_id" type="hidden" value="0"/>
                        <input name="sailingCharge" type="hidden" value="0"/>
                    </div>
                    <div class="form-group">
                        <label for="remarks">Remarks</label>
                        <textarea id="remarks" name="remarks" class="form-control" rows="4"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary sailingCharge">Submit</button>
            </div>
        </div>

    </div>
</div>
<script>

    function editSailingCharge(id) {

        $("#sailingChargeForm").trigger("reset");

        $.get("get_shipping_charges.php?get_one_sailing_charge=1&&charge_id="+id, function(data, status){
            let charge = JSON.parse(data);
            $("#sailingChargeForm").find('[name="invoice_no"]').val(charge.invoice_no);
            $("#sailingChargeForm").find('[name="description"]').val(charge.description);
            $("#sailingChargeForm").find('[name="amount"]').val(charge.amount);
            $("#sailingChargeForm").find('[name="remarks"]').val(charge.remarks);
            $("#sailingChargeForm").find('[name="container_id"]').val(charge.container_id);
          });

        $('#addSailingCharge').modal('show');
        $("#addSailingCharge").find(".modal-title").html("Edit Sailing Charge");
        $("#sailingChargeForm").find('[name="id"]').val(id);
    }

    $(document).on("click",".sailingCharge",function () {

        let data = {
            description: $("#sailingChargeForm").find('[name="description"]').val(),
            invoice_no: $("#sailingChargeForm").find('[name="invoice_no"]').val(),
            amount: $("#sailingChargeForm").find('[name="amount"]').val(),
            container_id: $("#sailingChargeForm").find('[name="container_id"]').val(),
            charge_id: $("#sailingChargeForm").find('[name="id"]').val(),
            sailingCharge: 1,
            remarks: $("#sailingChargeForm").find('[name="remarks"]').val()
        };

        $.ajax({
            type: "post",
            url: "update_container.php",
            data: data,
            success: function(response){
                $("#addSailingCharge").find(".response").addClass("alert alert-info").html(response);
                $('#addSailingCharge').modal('hide');
                $("#addSailingCharge").find(".response").removeClass("alert alert-info").html("");
                $("#sailingChargeForm").trigger("reset");

                getSailingCharges(data.container_id);
            }
        });

    });

    function getSailingCharges(container_id) {
        $.get("get_shipping_charges.php?get_all_sailing_charge=1&&container_id="+container_id, function(data, status){
            let list = JSON.parse(data);
            let html_data = "";

            list.forEach(function(item,index){
                html_data += "<tr><td>"+item.description+"</td><td>"+item.invoice_no+"</td><td>"+item.amount+"</td><td>"+item.remarks+"</td><td> <a href='#' onclick='editSailingCharge("+item.id+")' class='btn btn-info'>Edit</a> <a href='#' onclick='deleteSailingCharge("+item.id+","+item.container_id+")' class='btn btn-danger'>Delete</a></td></tr>";
            });

            $(".sailing_charges_tbody").html(html_data);
          });
    }

    function addSailingCharge(container_id) {
        $('#addSailingCharge').modal('toggle');
        $("#addSailingCharge").find(".modal-title").html("Add Sailing Charge");
        $("#sailingChargeForm").trigger("reset");
        $("#sailingChargeForm").find('[name="id"]').val("0")
        $("#sailingChargeForm").find(".container_id").val(container_id);
    }

    function deleteSailingCharge(id,container_id) {
        if (confirm("Proceed to delete?")){
            $.get("get_shipping_charges.php?delete_sailing_charge=1&&charge_id="+id, function(data, status){
                getSailingCharges(container_id);
            });
        }
    }

    // payments starts here

    function editPayment(id) {

        $("#addPaymentForm").trigger("reset");

        $.get("get_container_payments.php?get_one_payment=1&&payment_id="+id, function(data, status){
            let payment = JSON.parse(data);
            $("#addPaymentForm").find('[name="date"]').val(payment.date);
            $("#addPaymentForm").find('[name="description"]').val(payment.description);
            $("#addPaymentForm").find('[name="amount"]').val(payment.amount);
            $("#addPaymentForm").find('[name="remarks"]').val(payment.remarks);
            $("#addPaymentForm").find('[name="container_id"]').val(payment.container_id);
          });

        $('#addPayment').modal('show');
        $("#addPayment").find(".modal-title").html("Edit Payment");
        $("#addPaymentForm").find('[name="id"]').val(id);
    }

    $(document).on("click",".addPayment",function () {

        let data = {
            description: $("#addPaymentForm").find('[name="description"]').val(),
            date: $("#addPaymentForm").find('[name="date"]').val(),
            amount: $("#addPaymentForm").find('[name="amount"]').val(),
            container_id: $("#addPaymentForm").find('[name="container_id"]').val(),
            payment_id: $("#addPaymentForm").find('[name="id"]').val(),
            payment: 1,
            remarks: $("#addPaymentForm").find('[name="remarks"]').val()
        };

        $.ajax({
            type: "post",
            url: "update_container.php",
            data: data,
            success: function(response){
                $("#addPayment").find(".response").addClass("alert alert-info").html(response);
                $('#addPayment').modal('hide');
                $("#addPayment").find(".response").removeClass("alert alert-info").html("");
                $("#addPaymentForm").trigger("reset");

                getPayments(data.container_id);
            }
        });

    });

    function getPayments(container_id) {
        $.get("get_container_payments.php?get_all_payments=1&&container_id="+container_id, function(data, status){
            let list = JSON.parse(data);
            let html_data = "";
            let total=0;

            list.forEach(function(item,index){
                total += parseFloat(item.amount);
                html_data += "<tr><td>"+item.date+"</td><td>"+item.description+"</td><td>"+item.amount+"</td><td>"+item.remarks+"</td><td> <a href='#' onclick='editPayment("+item.id+")' class='btn btn-info'>Edit</a> <a href='#' onclick='deletePayment("+item.id+","+item.container_id+")' class='btn btn-danger'>Delete</a></td></tr>";
            });

            html_data+="<tr><td>Total payment:</td><td></td><td>"+total+"</td><td></td><td></td></tr>";

            $(".payments_tbody").html(html_data);
          });
    }

    function addPayment(container_id) {
        $('#addPayment').modal('toggle');
        $("#addPayment").find(".modal-title").html("Add Payment");
        $("#addPaymentForm").trigger("reset");
        $("#addPaymentForm").find('[name="id"]').val("0")
        $("#addPaymentForm").find(".container_id").val(container_id);
    }

    function deletePayment(id,container_id) {
        if (confirm("Proceed to delete?")){
            $.get("get_container_payments.php?delete_payments=1&&payment_id="+id, function(data, status){
                getPayments(container_id);
            });
        }
    }

</script>