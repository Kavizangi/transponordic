<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

define("_VALID_PHP", true);
require_once("../../init.php");

if (!$user->is_Admin())
    redirect_to("../login.php");

switch(Filter::$action): case "ship":
$row = Core::getRowById(Core::contaTable, Filter::$id);

$shipper = Core::getUserByUsername($row->username);
$container_name =  $row->order_inv;
?>
<!DOCTYPE html>
<html dir="ltr" lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../uploads/favicon.png">

    <title>Tracking - <?php echo $row->order_inv; ?></title>
    <!-- This page plugin CSS -->

    <!-- Custom CSS -->
    <link href="../dist/css/mdb/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/container.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid" id="invoice-container">
    <table>
        <tbody>
        <tr>
            <td class="half-width">
                <?php echo ($core->logo) ? '<img class="logo" src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->site_name.'" />': $core->site_name;?>
            </td>
            <td class="half-width">
                <br/>
                <br/>
                <h4 class="commercial_invoice">LOADING LIST</h4>
            </td>
        </tr>
        <tr class="bordered-tr">
            <td>
                <h4 class="headers">Transponordic Logistics</h4>
                <p>Paltholmterresserne 58F</p>
                <p>3520 Farum, Denmark</p>
                <p>E-mail : info@transponordic.dk</p>
                <p>Phone : +4531834040</p>
            </td>
            <td>
                <table class="bordered-table ref-table">
                    <tr>
                        <td>Ref No :</td>
                        <td><?php echo $container_name;?></td>
                    </tr>
                    <tr>
                        <td>Date/Week :</td>
                        <td><?php echo date("d/m/Y");?> - # <?php echo $row->s_week; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="bordered-tr">
            <td>
                <h4 class="headers">Shipper :</h4>
                <?php
                    $shipper = Core::getOfficesByName($row->origin_off);
                ?>
                <p><?php echo $row->origin_off;?></p>
                <p>Phone : <?php echo @$shipper->phone_off;?></p>
                <p>City: <?php echo @$shipper->city;?><p>
                <p>Address: <?php echo @$shipper->address;?></p>
            </td>
            <td>
                <h4 class="headers">Consignee :</h4>
                <p><?php echo $row->r_name; ?></p>
                <p>Phone : <?php echo $row->r_phone; ?></p>
                <p>Email: <?php echo $row->r_email;?></p>
                <p>Address: <?php echo $row->r_address;?></p>
            </td>
        </tr>
        </tbody>
    </table>
    <br/>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Container No.</th>
            <th>Commodity</th>
            <th>Total Packages</th>
            <th>Package Type</th>
            <th>HS Code</th>
        </tr>
        </thead>
        <tbody>
        <tr class="no-border-tr">
            <td colspan="5"></td>
        </tr>
        <?php
        $nums=1;
        $sumador_total=0;
        $sql = $db->query("SELECT * FROM add_container, detail_container WHERE add_container.idcon=detail_container.idcon AND add_container.id='".Filter::$id."'");

        $i=1;
        while ($row=mysqli_fetch_array($sql))
        {
            $id=$row["id"];
            $detail_weight=$row['detail_weight'];
            $detail_qty=$row['detail_qty'];
            $detail_container=$row['detail_container'];
            $description=$row['s_description'];
            $shipmode=$row['ship_mode'];
            $incoterm=$row['incoterms'];
            $originport=$row['origin_port'];
            $destinationport=$row['destination_port'];
            $rcurren=$row['r_curren'];
            $rtax=$row['r_tax'];
            $insurances=$row['r_insurance'];
            $track=$row['order_inv'];
            $hs_code=$row['hs_code'];
            $package_type=$row['package_type'];

            $price=$row['price'];
            $precio_venta_f=number_format($price,2);//Formatting variables
            $precio_venta_r=str_replace(",","",$precio_venta_f);//Replace the commas
            $precio_total=$precio_venta_r*$detail_qty;
            $precio_total_f=number_format($precio_total,2);//Total price formatted
            $precio_total_r=str_replace(",","",$precio_total_f);//Replace the commas
            $sumador_total+=$precio_total_r;//Adder
            if ($nums%2==0){
                $clase="clouds";
            } else {
                $clase="silver";
            }
            ?>
            <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $container_name;?></td>
                <td><?php echo $detail_container;?></td>
                <td><?php echo $detail_qty; ?></td>
                <td><?php echo $package_type;?></td>
                <td><?php echo $hs_code;?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="7"></td>
        </tr>
        </tbody>
    </table>
    <br/>
    <br/>
    <br/>
    <table class="table-bordered">
        <tr>
            <td class="full-width" style="padding: 1%">
                <h4 class="comments">Comments</h4>
                <p>
                    If you have any questions or concerns, please contact us:
                    <br/> Phone: +4526655068
                    <br/> E-mail: eyaafi@transponordic.dk
                </p>
            </td>
        </tr>
    </table>
    <br/>
    <br/>
</div>
<script>
    printDiv("invoice-container");
    function printDiv(divName) {

        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = '<link rel="stylesheet" href="../assets/css/container.css"/><link rel="stylesheet" href="../dist/css/mdb/bootstrap.min.css"/>'+printContents;

        window.print();

        document.body.innerHTML = originalContents;

        setTimeout(function () {
            window.close();
        },2000);
    }
</script>
</body>
</html>
<?php
    break;
endswitch;
?>