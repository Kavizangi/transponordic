<?php


define("_VALID_PHP", true);
require_once("../../init.php");

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);

if (!$user->is_Admin())
    redirect_to("../login.php");

    $row = Core::getRowById(Core::contaTable, Filter::$id);

    ?>
    <!DOCTYPE html>
    <html dir="ltr" lang="en" ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="../../uploads/favicon.png">

        <title>Invoice Template</title>
        <!-- This page plugin CSS -->

        <!-- Custom CSS -->
        <link href="../dist/css/mdb/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/css/invoice.css" rel="stylesheet">

    </head>
    <body>
    <div class="container-fluid" id="invoice-container">
        <table>
            <tbody>
            <tr>
                <td class="half-width">
                    <?php echo ($core->logo) ? '<img class="logo" src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->site_name.'" />': $core->site_name;?>
                </td>
                <td class="half-width">
                    <br/>
                    <br/>
                    <h4 class="commercial_invoice">COMMERCIAL INVOICE</h4>
                </td>
            </tr>
            <tr class="bordered-tr">
                <td>
                    <h4 class="headers">Transponordic Logistics</h4>
                    <p>Paltholmterresserne 58F</p>
                    <p>3520 Farum, Denmark</p>
                    <p>E-mail : info@transponordic.dk</p>
                    <p>Phone : +4531834040</p>
                </td>
                <td>
                    <table class="bordered-table ref-table">
                        <tr>
                            <td>Ref No :</td>
                            <td><?php echo $row->order_inv; ?></td>
                        </tr>
                        <tr>
                            <td>Dated :</td>
                            <td><?php echo $row->expiration_date; ?></td>
                        </tr>
                        <tr>
                            <td>Currency :</td>
                            <td><?php echo $core->currency;?> </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="bordered-tr">
                <td>
                    <?php
                        $shipper = Core::getOfficesByName($row->origin_off);
                    ?>
                    <h4 class="headers">Shipper :</h4>
                    <p><?php echo $row->origin_off;?></p>
                    <p>Phone : <?php echo @$shipper->phone_off;?></p>
                    <p>City: <?php echo @$shipper->city;?><p>
                    <p>Address: <?php echo @$shipper->address;?></p>
                </td>
                <td>
                    <h4 class="headers">Consignee :</h4>
                    <p><?php echo $row->r_name;?></p>
                    <p>Phone : <?php echo $row->r_phone;?></p>
                    <p>Email: <?php echo $row->r_email;?></p>
                    <p>Address: <?php echo $row->r_address;?></p>
                </td>
            </tr>
            </tbody>
        </table>
        <br/>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <tr class="no-border-tr">
                <td colspan="5"></td>
            </tr>
            <?php
            $nums=1;
            $subtotal=0;
            $sumador_total=0;
            $sql = $db->query("SELECT * FROM add_container, detail_container WHERE add_container.idcon=detail_container.idcon AND add_container.id='".Filter::$id."'");

            $rrow=$row;
            $i=1;
            while ($row=mysqli_fetch_array($sql))
            {
                $id=$row["id"];
                $detail_weight=$row['detail_weight'];
                $detail_qty=$row['detail_qty'];
                $detail_container=$row['detail_container'];
                $description=$row['s_description'];
                $shipmode=$row['ship_mode'];
                $incoterm=$row['incoterms'];
                $originport=$row['origin_port'];
                $destinationport=$row['destination_port'];
                $rcurren=$row['r_curren'];
                $rtax=$row['r_tax'];
                $insurances=$row['r_insurance'];
                $track=$row['order_inv'];
                $hs_code=$row['hs_code'];
                $package_type=$row['package_type'];

                $price=$row['price'];
                $precio_venta_f=number_format($price,2);//Formatting variables
                $precio_venta_r=str_replace(",","",$precio_venta_f);//Replace the commas
                $precio_total=$precio_venta_r*$detail_qty;
                $precio_total_f=number_format($precio_total,2);//Total price formatted
                $precio_total_r=str_replace(",","",$precio_total_f);//Replace the commas
                $sumador_total+=$precio_total_r;//Adder
                if ($nums%2==0){
                    $clase="clouds";
                } else {
                    $clase="silver";
                }

                $subtotal += $row['tprice'];
                ?>
                <tr>
                    <td><?php echo $i++;?></td>
                    <td><?php echo $detail_container;?></td>
                    <td><?php echo $detail_qty; ?></td>
                    <td><?php echo $price;?></td>
                    <td><?php echo $row['tprice'];?></td>
                </tr>
                <?php
            }

            $totaltax = $subtotal * ($rrow->r_tax > 0 ? $rrow->r_tax : 1)/100;
            ?>
            <tr>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td>Sub-total</td>
                <td> <?php echo number_format($subtotal); ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td>Tax (<?php echo $rrow->r_tax;?>%)</td>
                <td><?php echo number_format(round_out($totaltax)); ?></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td>Total</td>
                <td><?php
                    $total=$totaltax+$subtotal;
                    echo number_format(round_out($total)); ?></td>
            </tr>
            <tr>
                <td colspan="5">
                    <h4 class="text-left" style="font-size: 14px;">Total Amount :</h4>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="text-left"><?php echo @numberToWords(round_out($total));?></td>
                <td></td>
                <td><?php echo number_format(round_out($total)); ?></td>
            </tr>
            </tbody>
        </table>
        <table class="table-bordered">
            <tr>
                <td class="half-width" style="padding: 1%">
                    <p>Payment Information :</p>
                    <p>Amount payable to : <br/><strong>Transponordic Logistics</strong></p>
                    <br/>
                    <p>
                        REG: 0111<br/>
                        KONTONR: 6894234871<br/>
                        NORDEA BANK<br/>
                        The Invoice is payable within 7 days of issue.
                    </p>
                </td>
                <td class="half-width" style="padding: 1%">
                    <p>E & O.E.<br/>
                        For Transponordic Logistics</p>
                </td>
            </tr>
        </table>
        <hr/>
        <center>
            <p>Transponordic Logistics | Service to Humanity</p>
        </center>
    </div>
    <script>
        printDiv("invoice-container");
        function printDiv(divName) {

            let printContents = document.getElementById(divName).innerHTML;
            let originalContents = document.body.innerHTML;

            document.body.innerHTML = '<link rel="stylesheet" href="../assets/css/invoice.css"/><link rel="stylesheet" href="../dist/css/mdb/bootstrap.min.css"/>'+printContents;

            window.print();

            document.body.innerHTML = originalContents;

            setTimeout(function () {
                window.close();
            },2000);
        }
    </script>
    </body>
    </html>
