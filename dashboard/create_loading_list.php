<?php

if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

$user = Core::getRowById(Users::uTable, Filter::$id);

?>
    <link href="dist/assets/css_log/front.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="dist/assets/js/jquery.js"></script>
    <script type="text/javascript" src="dist/assets/js/jquery-ui.js"></script>
    <script src="dist/assets/js/jquery.ui.touch-punch.js"></script>
    <script src="dist/assets/js/jquery.wysiwyg.js"></script>
    <script src="dist/assets/js/global.js"></script>
    <script src="dist/assets/js/custom.js"></script>

<?php switch(Filter::$action): case "edit": ?>
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">

            </div>
        </div>
        <!-- Column -->
    </div>
    <?php break;?>
<?php default:
    $containers = Core::getContainersForSender($user->username);
?>

    <div class="row justify-content-center">
        <!-- Column -->
        <div class="col-lg-8 col-xlg-12 col-md-12">
            <div class="card">
                <div class="table-responsive">

                    <table id="zero_config" class="table table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th><b>Tracking</b></th>
                            <th class="text-center"><b>Customer Name</b></th>
                            <th class="text-center"><b>Origin Port</b></th>
                            <th class="text-center"><b>Destination Port</b></th>
                            <th class="text-center"><b>Shipping Line</b></th>
                            <th class="text-center"><b>Payment</b></th>
                            <th class="text-center"><b>Container Status</b></th>
                        </tr>
                        </thead>
                        <div class="m-t-40">
                            <div class="d-flex">
                                <div class="mr-auto">
                                    <div class="form-group">
                                        <a href="loading.php?do=create_loading_list"><button type="button" class="btn btn-primary btn"><i class="ti-plus" aria-hidden="true"></i> Create Loading List</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <tbody id="projects-tbl">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>

    <?php break;?>

<?php endswitch;?>