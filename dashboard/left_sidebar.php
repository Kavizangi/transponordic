
<style>
    .sidebar-nav ul .sidebar-item .sidebar-link {
        color: #212529!important;
    }
</style>
	   <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
				 <?php
                    if($row->userlevel == 9){
                        require_once("admin_left_bar.php");
                    }else if($row->userlevel == 1){
                        require_once("customer_left_bar.php");
                    }
                 ?>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
